#include "SystemManager.h"

void SystemManager::init()
{
	for (size_t i = 0; i < systems.size(); i++)
		systems[i]->init();
}

//void SystemManager::preUpdate(double deltaTime)
//{
//	for (size_t i = 0; i < systems.size(); i++)
//		systems[i]->preUpdate(deltaTime);
//}

void SystemManager::update(double deltaTime)
{
	for (size_t i = 0; i < systems.size(); i++)
	{
		//cout << "Size " << typeid(*systems[i]).name() << " ECount" << systems[i]->entityCount() << " Comps" << systems[i]->compsTohave << endl;
		systems[i]->update(deltaTime);
	}
}

//void SystemManager::postUpdate(double deltaTime)
//{
//	for (size_t i = 0; i < systems.size(); i++)
//		systems[i]->postUpdate(deltaTime);
//}

ISystem* SystemManager::addSystem(ISystem* s)
{
	systems.push_back(s);
	return s;
}

void SystemManager::removeSystem(ISystem* s)
{
	//systems.erase(s);
}

void SystemManager::addEntity(Entity e)
{
	for (size_t i = 0; i < systems.size(); i++)
		systems[i]->addEntity(e);
}

void SystemManager::removeEntity(Entity e)
{
	for (size_t i = 0; i < systems.size(); i++)
		systems[i]->removeEntity(e);
}
