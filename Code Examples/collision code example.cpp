struct Point
{
	Point() { x = y = 0;  }
	Point(float px, float py) 	{  x = px;  y = py; }

	Point operator+(const Point &A)
	{
		return Point(x + A.x, y + A.y);
	}

	Point operator-(const Point &A)
	{
		return Point(x - A.x, y - A.y);
	}

	Point operator*(float f) const
	{
		return Point(x * f, y * f);
	}

	float x;
	float y;
};

struct Line
{
	Line(float x1, float y1, float x2, float y2)
	{
		Ax = x1;	Ay = y1;
		Bx = x2;	By = y2;
	}

	float Ax;
	float Ay;
	float Bx;
	float By;
};

struct Box
{
	float x;
	float y;
	float width;
	float height;
};

struct Circle
{
	// x,y are the center
	float x;
	float y;
	float radius;
};

// Returns the distance between two Points
float Distance_PointPoint(Point A, Point B)
{
	return sqrt(pow((B.x - A.x), 2) + pow((B.y - A.y), 2));
}

bool AABB_BoxBox(Box A, Box B)
{
	float Axmin = A.x;
	float Axmax = A.x + A.width;
	float Aymax = A.y + A.height;
	float Aymin = A.y;

	float Bxmin = B.x;
	float Bxmax = B.x + B.width;
	float Bymax = B.y + B.height;
	float Bymin = B.y;

	if (Axmax < Bxmin || Axmin > Bxmax) return false;
	if (Aymax < Bymin || Aymin > Bymax) return false;

	return true;
}

bool AABB_BoxPoint(Box B, Point P)
{
	return (P.x >= B.x) && (P.x < B.x + B.width) &&
		   (P.y >= B.y) && (P.y < B.y + B.height);
}

bool AABB_BoxCircle(Box B, Circle C)
{
	float Dx = C.x - std::max(B.x, std::min(C.x, B.x + B.width));
	float Dy = C.y - std::max(B.y, std::min(C.y, B.y + B.height));

	return (Dx * Dx + Dy * Dy) < (C.radius * C.radius);
}

bool AABB_LineLine(Line A, Line B)
{
	Point b = Point(A.Bx, A.By) - Point(A.Ax, A.Ay);
	Point d = Point(B.Bx, B.By) - Point(B.Ax, B.Ay);
	float DotDPerp = b.x * d.y - b.y * d.x;

	// if DotdPerp == 0, it means the lines are parallel
	if (DotDPerp == 0) return false;

	Point c = Point(B.Ax, B.Ay) - Point(A.Ax, A.Ay);
	float t = (c.x * d.y - c.y * d.x) / DotDPerp;
	if (t < 0 || t > 1) return false;

	float u = (c.x * b.y - c.y * b.x) / DotDPerp;
	if (u < 0 || u > 1) return false;

	return true;
}

bool AABB_BoxLine(Box B, Line L)
{
	float Xmin, Xmax, Ymin, Ymax;
	Xmin = B.x;
	Xmax = B.x + B.width;
	Ymin = B.y;
	Ymax = B.y + B.height;

	if (L.Ax < Xmin && L.Bx < Xmin) return false;
	if (L.Ay < Ymin && L.By < Ymin) return false;
	if (L.Ax > Xmax && L.Bx > Xmax) return false;
	if (L.Ay > Ymax && L.By > Ymax) return false;

	// check every side of the rectangle
	if (AABB_LineLine(L, Line(Xmin, Ymin, Xmax, Ymin))) return true;
	if (AABB_LineLine(L, Line(Xmin, Ymin, Xmin, Ymax))) return true;
	if (AABB_LineLine(L, Line(Xmax, Ymin, Xmax, Ymax))) return true;
	if (AABB_LineLine(L, Line(Xmin, Ymax, Xmax, Ymax))) return true;

	return false;
}

bool AABB_CircleCircle(Circle A, Circle B)
{
	float dx = B.x - A.x;
	float dy = A.y - B.y;
	float R = A.radius + B.radius;

	return (dx*dx + dy*dy <= R*R);
}

bool AABB_CircleLine(Circle C, Line L)
{
	// Circle centre point
	Point P(C.x, C.y);

	float APx = P.x - L.Ax;
	float APy = P.y - L.Ay;

	float ABx = L.Bx - L.Ax;
	float ABy = L.By - L.Ay;

	float magAB2 = ABx*ABx + ABy*ABy;
	float ABdotAP = ABx*APx + ABy*APy;

	float t = ABdotAP / magAB2;

	// Closest point to the line L from Circle center
	Point W;

	if (t < 0)	 W = Point(L.Ax, L.Ay);
	else if (t > 1)  W = Point(L.Bx, L.By);
	else			 W = Point(L.Ax + ABx * t, L.Ay + ABy * t);

	float dist = Distance_PointPoint(P, W);

	// if the distance is less than the circle radius
	return dist < C.radius;
}

bool AABB_CirclePoint(Circle C, Point P)
{
	float dx = std::abs(P.x - C.x);
	float dy = std::abs(P.y - C.y);
	float R = C.radius;

	return (dx*dx + dy*dy <= R*R);
}