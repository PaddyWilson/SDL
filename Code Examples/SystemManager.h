#pragma once//
#include <vector>

#include "ISystem.h"
class SystemManager
{
public:
	void init();

	//void preUpdate(double deltaTime);
	void update(double deltaTime);
	//void postUpdate(double deltaTime);

	ISystem addSystem(ISystem* s);
	void removeSystem(ISystem* s);

	void addEntity(Entity e);
	void removeEntity(Entity e);

protected:
	vector<ISystem*> systems;
};