#include "World.h"

#include "CompIncludes.h"
World::World() :
	componentManagers(AMOUNT_OF_COMPONENTS), componentManagerNames(AMOUNT_OF_COMPONENTS)
{
	createdEventSystem = true;
	eventSystem = new EventSystem();
}

World::World(EventSystem * eventSystem) :
	componentManagers(AMOUNT_OF_COMPONENTS)
{
	this->createdEventSystem = false;
	this->eventSystem = eventSystem;
}

World::~World()
{
	//clean up component managers
	for (int i = 0; i < componentManagers.size(); i++)
	{
		if (componentManagers[i] != NULL)
		{
			delete componentManagers[i];
			componentManagers[i] = NULL;
		}
	}

	//clean up systems
	for (int i = 0; i < systems.size(); i++)
	{
		if (systems[i] != NULL)
		{
			delete systems[i];
			systems[i] = NULL;
		}
	}

	if (createdEventSystem)
		delete eventSystem;
}

void World::init()
{
	for (int i = 0; i < systems.size(); i++)
	{
		systems[i]->init();
	}
}

EntityHandle World::createEntity()
{
	EntityHandle handle;//create new handle
	bitset<AMOUNT_OF_COMPONENTS> bits;//create new bitmask
	handle.world = this;
	entityBits.emplace(handle.entity.id, bits);//map bitmask to entity
	addQueue.emplace(handle.entity);//add entity to addQueue
	return handle;
}

EntityHandle World::getEntity(int id)
{
	EntityHandle handle;
	handle.world = this;
	handle.entity = Entity(id);//entityManager.get(id);
	return handle;
}

void World::removeEntity(Entity e)
{
	removeQueue.emplace(e);
}

void World::addSystem(ISystem * system)
{
	system->setWorld(this);
	system->setEventSystem(eventSystem);
	systems.push_back(system);
}

void World::preUpdate(double deltaTime)
{
	//add and remove entity from the world
	processRemove();
	processAdd();

	//systemManager.preUpdate(deltaTime);
}

void World::update(double deltaTime)
{
	preUpdate(deltaTime);

	for (int i = 0; i < systems.size(); i++)
	{
		systems[i]->update(deltaTime);
	}

	postUpdate(deltaTime);
}

void World::postUpdate(double deltaTime)
{
	//systemManager.postUpdate(deltaTime);
}

void World::addTag(Entity e, string tag)
{
	tagMgr.add(e, tag);
}

EntityHandle World::getTag(string tag)
{
	EntityHandle handle;
	handle.entity = tagMgr.get(tag);
	handle.world = this;
	return handle;
	//return EntityHandle();
}

void World::removeTag(Entity e)
{
	tagMgr.remove(e);
}

void World::removeTag(string tag)
{
	tagMgr.remove(tag);
}

void World::addGroup(Entity e, string group)
{
	groupMgr.add(e, group);
}

vector<Entity>* World::getGroup(Entity e)
{
	return groupMgr.get(e);
}

vector<Entity>* World::getGroup(string group)
{
	return groupMgr.get(group);
}

void World::removeFromGroup(Entity e)
{
	groupMgr.remove(e);
}

void World::removeFromGroup(Entity e, string group)
{
	groupMgr.remove(e, group);
}

void World::removeGroup(string group)
{
	groupMgr.removeGroup(group);
}

//void World::addPrefab(string name, function<EntityHandle(Vector2D)> function)
//{
//}
//
//EntityHandle World::createPrefab(string name, Vector2D position)
//{
//	return EntityHandle();
//}

void World::addPrefab(string tag, function<EntityHandle(float, float)> function)
{
	prefabMap[tag] = function;
}

EntityHandle World::createPrefab(string tag, float posx, float posy)
{
	if (prefabMap.count(tag) == 0)
		return EntityHandle();
	return prefabMap[tag](posx, posy);
}

bitset<AMOUNT_OF_COMPONENTS> World::getEntityBitmask(Entity e)
{
	if (entityBits.count(e.id) == 0)
		return bitset<AMOUNT_OF_COMPONENTS>();
	return entityBits[e.id];
}

int World::getEntityCount()
{
	return entityManager.size();
}

int World::componentCount(int compId)
{
	if (componentManagers[compId] != nullptr)
		return componentManagers[compId]->size();
	return 0;
}

void World::processAdd()
{
	while (!addQueue.empty())
	{
		entityManager.add(addQueue.front());
		for (size_t i = 0; i < systems.size(); i++)
		{
			systems[i]->addEntity(addQueue.front());
		}
		addQueue.pop();
	}
}

void World::processRemove()
{
	while (!removeQueue.empty())
	{
		entityManager.remove(removeQueue.front());
		for (size_t i = 0; i < systems.size(); i++)
		{
			systems[i]->removeEntity(removeQueue.front());
		}

		//remove components from the managers
		for (size_t i = 0; i < componentManagers.size(); i++)
		{
			if (componentManagers[i] != NULL)
				componentManagers[i]->remove(removeQueue.front());
		}

		tagMgr.remove(removeQueue.front());
		groupMgr.remove(removeQueue.front());
		entityBits.erase(removeQueue.front().id);

		removeQueue.pop();
	}
}
