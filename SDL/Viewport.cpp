
#include "Viewport.h"


Viewport::Viewport(int x, int y, int width, int height)
{
	view.x = x;
	view.y = y;
	view.w = width;
	view.h = height;
}

Viewport::~Viewport(){}

void Viewport::switchView(SDL_Renderer * renderer)
{
	SDL_RenderSetViewport(renderer, &view);
}

int Viewport::getX()
{
	return view.x;
}

int Viewport::getY()
{
	return view.y;
}

int Viewport::getWidth()
{
	return view.w;
}

int Viewport::getHeight()
{
	return view.h;
}
