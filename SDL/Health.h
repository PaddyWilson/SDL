#pragma once

#include "Component.h"

struct Health : public Component<Health>
{
	Health() :health(0) {};
	Health(float health) :health(health) {};

	float health;
};