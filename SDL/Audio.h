#pragma once
#include <SDL.h>
#include <SDL_mixer.h>
#include <string>

#include "Global.h"

#include <iostream>

class Music
{
private:
	Mix_Music * music;

protected:
	int loop = -1;
	int channel = -1;
public:
	Music(int loop = -1, int channel = -1);
	~Music();

	virtual void play();
	virtual void pause();
	virtual void resume();
	virtual void stop();

	virtual bool load(std::string path);
};

class SoundEffect :public Music
{
private:
	Mix_Chunk * sound;

	//int channel = -1;
	//int loop = 0;
public:
	SoundEffect(int loop = 0, int channel = -1);
	~SoundEffect();

	void play() override;
	void pause() override {};	//cant be paused
	void resume() override {};	//or resumed
	void stop() override {};	//or stopped

	bool load(std::string path) override;
};


