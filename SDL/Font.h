#pragma once
#include <SDL.h>
#include <SDL_ttf.h>
#include <string>
#include <map>
#include <vector>

#include "Texture.h"

class Font
{
private:
	std::string location;
	std::map<int, TTF_Font*> fonts;

	std::vector<SDL_Texture*> textures;

public:
	void loadFont(std::string file);
	//call when done using the font
	void unloadFont();

	void fontSize(int size);

	SDL_Texture* write(SDL_Renderer* renderer, std::string text, int x, int y, int size = 20, SDL_Color color = SDL_Color{ 255,255,255,255 });
	Texture* createTexture(SDL_Renderer* renderer, std::string text, int size = 20, SDL_Color color = SDL_Color{ 255,255,255,255 });

	//call every frame to clean up created textures
	void clearTextures();
};

