#include "EntityManager.h"

Entity EntityManager::createEntity()
{
	Entity e;
	//entities.push_back(e);
	return e;
}

Entity EntityManager::add(Entity e)
{
	entities.push_back(e);
	return e;
}

Entity EntityManager::get(int id)
{
	return entities[id];
}

Entity EntityManager::getIndex(int index)
{
	return entities[index];
}

void EntityManager::remove(Entity e)
{
	for (int i = 0; i < entities.size(); i++)
	{
		if (entities[i].id == e.id)
		{
			entities.erase(entities.begin() + i);
		}
	}
}

//void EntityManager::processAdd()
//{
//	while (!addQueue.empty())
//	{
//		entities.push_back(addQueue.front());
//		addQueue.pop();
//	}
//}

//void EntityManager::processRemove()
//{
//	while (!removeQueue.empty())
//	{
//		auto item = std::find(entities.begin(), entities.end(), removeQueue.front());
//
//		if (item == entities.end())
//			break;
//
//		entities.erase(item);
//		removeQueue.pop();
//	}
//}

int EntityManager::size()
{
	return (int)entities.size();
}
