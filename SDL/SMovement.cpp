
#include "SMovement.h"

SMovement::SMovement()
{
	requiredComponents.set(getComponentID<Transform>(), true);
	requiredComponents.set(getComponentID<Movement>(), true);
}

SMovement::~SMovement() {}

void SMovement::init() {}

void SMovement::update(double dt)
{
	for (size_t i = 0; i < entities.size(); i++)
	{
		Transform*t = world->getComponent<Transform>(entities[i]);
		Movement*m = world->getComponent<Movement>(entities[i]);

		m->velocity += m->accecleration * dt;
		t->position += m->velocity * dt;
	}
}