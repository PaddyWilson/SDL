#pragma once
#include "Event.h"

#include "EntityHandle.h"

struct CollisionEvent :public Event
{
	CollisionEvent(EntityHandle e1, EntityHandle e2) :
		e1(e1), e2(e2) {};

	EntityHandle e1, e2;
};