#pragma once

#include "ISystem.h"
#include "Animation.h"
#include "CompIncludes.h"

class AnimationSystem : public ISystem
{
public:
	AnimationSystem();
	void init() override;
	void update(double dt) override;

	void changeAnimation(Entity entity, string animation);
};
