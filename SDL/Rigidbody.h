#pragma once
#include "Component.h"
enum BodyType
{
	Dynamic, Kinematic, Static
};
struct Rigidbody
{
	Rigidbody();
	~Rigidbody();

	BodyType bodyType;
};

