#pragma once

#include <map>
#include <vector>

#include <iostream>

#include "World.h"
#include "Entity.h"
#include "EntityHandle.h"
#include "Component.h"

#include "Event.h"

using namespace std;

struct EntityHandler;
class World;

class ISystem
{
protected:
	int updateOrder;
	vector<Entity> entities;

	bitset<AMOUNT_OF_COMPONENTS> requiredComponents;
	bitset<AMOUNT_OF_COMPONENTS> optionalComponents;
	
	World* world;
	EventSystem *eventSystem;

public:
	virtual void init() {};

	virtual void update(double deltaTime) {};

	void setWorld(World * world);
	void setEventSystem(EventSystem *eventSystem);

	bool hasComponents(Entity e);
	virtual void addEntity(Entity e);
	virtual void removeEntity(Entity e);

	inline int entityCount() { return entities.size(); };
};