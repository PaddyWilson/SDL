#pragma once
#include <map>
#include <string>

#include "Global.h"
#include "Audio.h"

using namespace std;

class AudioManager
{
private:
	//std::map<std::string, SoundEffect> soundEffects;
	std::map<std::string, Music*> music;

public:
	AudioManager();
	~AudioManager();

	bool load(string name, string path, bool isMusic = false, bool fromContent = true);

	void play(string name);
	void pause();
	void resume();
	void stop();

	bool add(string name, string path, bool isMusic = false, bool fromContent = true);
	Music* get(string name);
	void remove(string name);

	int getCount();
};

extern AudioManager gAudioMgr;
