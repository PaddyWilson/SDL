#include "AnimationSystem.h"

AnimationSystem::AnimationSystem()
{
	requiredComponents.set(getComponentID<SpriteRenderer>(), true);
	requiredComponents.set(getComponentID<CAnimation>(), true);
}

void AnimationSystem::init()
{
}

void AnimationSystem::update(double dt)
{
	for (size_t i = 0; i < entities.size(); i++)
	{
		EntityHandle e;
		e.entity = entities[i];
		e.world = world;

		CAnimation *animation = e.getComponent<CAnimation>();
		SpriteRenderer *sr = e.getComponent<SpriteRenderer>();

		//current animation not set
		if (animation->currentAnimation == "")
		{
			for (auto item : animation->animations)
			{
				animation->currentAnimation = item.first;
				return;
			}
		}
		else if (animation->lastAnimation != animation->currentAnimation)
		{
			if (animation->animations[animation->currentAnimation].isPaused())
			{
				//start animation if it was paused
				animation->animations[animation->currentAnimation].start();
			}
			else
			{
				//stop last animation
				animation->animations[animation->lastAnimation].stop();
				animation->lastAnimation = animation->currentAnimation;
				//start new animation
				animation->animations[animation->currentAnimation].start();
			}

		}

		if (animation->animations.size() != 0)
			sr->sprite = animation->animations[animation->currentAnimation].getNextFrame()->sprite;
	}
}

void AnimationSystem::changeAnimation(Entity entity, string animation)
{
	EntityHandle e;
	e.entity = entity;
	e.world = world;

	CAnimation *an = e.getComponent<CAnimation>();

	//no animation comp
	if (an == nullptr) return;

	// entity has the animation
	if (an->animations.count(animation) != 0)
	{
		an->currentAnimation = animation;
	}
}

