#pragma once
#include <map>
#include <vector>
#include <memory>
#include <string>

#include "Entity.h"

using namespace std;

class GroupManager
{
private:
	map<string, vector<Entity>*> groups;

	//use this so their are no memory leaks
	vector<Entity> emptyGroup;

public:
	~GroupManager();

	void add(Entity e, string group);

	vector<Entity>* get(Entity e);
	vector<Entity>* get(string group);

	vector<string> getGroupList();

	void remove(Entity e);
	void remove(Entity e, string group);
	void removeGroup(string group);

	void removeAll();
	int size();
	int size(string group);
};
