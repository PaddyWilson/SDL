#include "Sprite.h"
#include "TextureManager.h"
#include <iostream>
Sprite::Sprite() {}

Sprite::Sprite(std::string texture, int x, int y, int width, int height)
{
	SDL_Rect r;
	r.x = x;
	r.y = y;
	r.w = width;
	r.h = height;
	init(texture, r);
}

Sprite::Sprite(std::string texture, SDL_Rect subRect)
{
	init(texture, subRect);
}

Sprite::~Sprite() {
	//cout << "~Sprite()" << endl;
}

void Sprite::render(SDL_Renderer * renderer, float x, float y)
{
	SDL_Rect r;
	r.x = x;
	r.y = y;
	r.w = width;
	r.h = height;

	SDL_RenderCopy(renderer, texture->getTexture(), &subRect, &r);
}

void Sprite::render(SDL_Renderer * renderer, Vector2D vec)
{
	SDL_Rect r;
	r.x = vec.x;
	r.y = vec.y;
	r.w = width;
	r.h = height;

	SDL_RenderCopy(renderer, texture->getTexture(), &subRect, &r);
}

void Sprite::init(std::string textureName, SDL_Rect subRect)
{
	this->textureName = textureName;
	this->texture = gTextureManager.getTexture(textureName);
	this->subRect = subRect;

	//if (texture == NULL)
	//{
	//	//cout << "Sprite: NULL" << endl;
	//	return;
	//}

	//no sub rect use texture width and height
	if (subRect.x <= 0 && subRect.y <= 0 && subRect.w <= 0 & subRect.h <= 0)
	{
		this->width = texture->getWidth();
		this->height = texture->getHeight();
		//subRect is size of texture
		this->subRect.x = 0;
		this->subRect.y = 0;
		this->subRect.w = width;
		this->subRect.h = height;
	}
	else
	{
		this->width = subRect.w;
		this->height = subRect.h;
	}
}