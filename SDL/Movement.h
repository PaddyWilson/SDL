#pragma once
#include "Component.h"
#include "Vector2D.h"

struct Movement : public Component<Movement>
{
	Vector2D accecleration;
	Vector2D velocity;

	Vector2D maxSpeed;
};
