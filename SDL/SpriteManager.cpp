
#include "SpriteManager.h"

SpriteManager gSpriteManager;

SpriteManager::SpriteManager() {}

SpriteManager::~SpriteManager() {}

Sprite * SpriteManager::create(std::string name, std::string texture, int x, int y, int width, int height)
{
	//printf("spriteManager create()\n");
	if (sprites.count(name) != 0)
	{
		//printf("spriteManager create() sprite found \n");
		//return &sprites[name];
		std::cout << "Over writing sprite:" << name << " from Texture:" << texture << std::endl;
	}
	else
	{
		spriteNames.push_back(name);
	}

	Sprite s(texture, x, y, width, height);
	sprites[name] = s;
	return &sprites[name];
}

void SpriteManager::addSprite(std::string name, Sprite &sprite)
{
	if (sprites.count(name) != 0)
	{
		std::cout << "Over writing sprite:" << name << std::endl;
		//printf("Warning: Dupe sprite name (%s) overwriting", name);
	}
	else
	{
		spriteNames.push_back(name);
	}
	sprites[name] = sprite;
}

Sprite * SpriteManager::getSprite(std::string name)
{
	//printf("spriteManager get()\n");
	if (sprites.count(name) == 0)
	{
		std::cout << "getSprite() Sprite not found:" << name << std::endl;
		return NULL;
	}

	return &sprites[name];
}

void SpriteManager::removeSprite(std::string name)
{
	if (sprites.count(name) != 0) {
		sprites.erase(name);
		auto it = std::find(spriteNames.begin(), spriteNames.end(), name);
		spriteNames.erase(it);
	}
}

int SpriteManager::getCount()
{
	return sprites.size();
}

std::vector<std::string>* SpriteManager::getNames()
{
	return &spriteNames;
}
