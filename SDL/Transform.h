#pragma once

#ifndef TRANSFORM_H 
#define TRANSFORM_H

#include "Component.h"
#include "Vector2D.h"

struct Transform : public Component<Transform>//, public Vector2D
{

	Transform(float x = 0.0f, float y = 0.0f)
	{
		position.x = x;
		position.y = y;
	}

	Transform(Vector2D vec)
	{
		position = vec;
	}

	//part of struct
	//float x, y;
	Vector2D position;

	float rotation = 0;
	float scale = 1;
};

#endif