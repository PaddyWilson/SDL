#pragma once

#define AMOUNT_OF_COMPONENTS 32

#include <typeinfo>
#include <map>
#include <string>
#include <bitset>
using namespace std;

struct ComponentID
{
	static int counter;
};

struct BaseComponent{};

template<typename T>
struct Component : public BaseComponent
{
	//returns a unique id for each component type
	static inline int id()
	{
		static int id = ComponentID::counter++;
		return id;
	}

	//returns a unique bit for each component type
	static inline bitset<AMOUNT_OF_COMPONENTS> bit()
	{
		static bitset<AMOUNT_OF_COMPONENTS> bit = bitset<AMOUNT_OF_COMPONENTS>().set(id(), true);
		return bit;
	}

	static string name()
	{
		static string name = typeid(T).name();
		return name;
	}
};

template<typename T>
static int getComponentID() 
{
	return Component<T>::id();
}

template<typename T>
static bitset<AMOUNT_OF_COMPONENTS> getComponentBit()
{
	return Component<T>::bit();
}

template<typename T>
static string getComponentName()
{
	return Component<T>::name();
}