#include "Helper.h"

std::vector<std::string> getFilesInFolder(std::string folder)
{
	namespace fs = std::experimental::filesystem;
	std::vector<std::string> files;
	for (auto & p : fs::directory_iterator(folder))
	{
		files.push_back(p.path().generic_string());
	}
	return files;
}

std::vector<std::string> splitString(std::string text, char c)
{
	std::stringstream test(text);
	std::string segment;
	std::vector<std::string> seglist;
	while (std::getline(test, segment, c))
	{
		seglist.push_back(segment);
	}
	return seglist;
}