#pragma once
#include <map>
#include <typeindex>
#include <vector>

//event base class
class Event {};

//used to be able to have pointers to the EventHandle class
class EventHandleBase
{
private:
	virtual void call(Event * event) = 0;
public:
	void execute(Event *event)
	{
		call(event);
	}
};

//holds a referace to a class and funtion in that class to be called
template<class T, class EventType>
class EventHandle : public EventHandleBase
{
public:
	typedef void (T::*MemberFunction)(EventType*);

	EventHandle(T*instance, MemberFunction function) :
		instance(instance), function(function) {};

	void call(Event* event)
	{
		(instance->*function)(static_cast<EventType*>(event));
	}

private:
	T * instance;//class/struct pointer
	MemberFunction function;//pointer to function in class
};

//used to sub, unsub, and publish events
//class all the subs that are subed
class EventSystem
{
public:
	typedef std::vector<EventHandleBase*> EventList;

	//~EventSystem();

	template<typename EventType> void publish(EventType *event);
	template<class T, class EventType> void subscribe(T* instance, void (T::*function)(EventType *));
	//currently does not unsub from events
	template<class T, class EventType> void unsubscribe(T* instance, void (T::*function)(EventType *));

private:	
	std::map<std::type_index, EventList*> subs;
};
//
//EventSystem::~EventSystem()
//{
//	//for (auto sub : subs) {
//	//	if (sub.second != nullptr)
//	//	{
//	//		//delete items from EventList
//	//		//for (auto &item : *sub.second)
//	//			//delete item;
//	//		//delete the EventList
//	//		//delete sub.second;
//	//	}
//	//}
//}

template<typename EventType>
void EventSystem::publish(EventType * event)
{
	EventList* events = subs[typeid(EventType)];

	//no subs for this event
	if (events == nullptr)
		return;

	//call all the subs of the event
	for (auto & evt : *events)
	{
		if (evt != nullptr)
			evt->execute(event);
	}
}

template<class T, class EventType>
void EventSystem::subscribe(T * instance, void(T::* function)(EventType *))
{
	std::cout << "sub 1 " << typeid(EventType).name() << " " << subs.size() << endl;
	int i = 0;
	i++;
	std::type_index a = typeid(EventType);
	EventList* events = subs[typeid(EventType)];

	//create a new EventList if their are none
	if (events == nullptr)
	{
		std::cout << "sub 1.1" << endl;
		events = new EventList();
		subs[typeid(EventType)] = events;
	}
	std::cout << "sub 2" << endl;
	//add the function to the list
	events->push_back(new EventHandle<T, EventType>(instance, function));
}

template<class T, class EventType>
void EventSystem::unsubscribe(T * instance, void(T::* function)(EventType *))
{
	EventList* events = subs[typeid(EventType)];

	//no events with that type
	if (events == nullptr)
		return;

	//function not done
}
