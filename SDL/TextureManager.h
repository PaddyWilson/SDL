#pragma once

#include <SDL.h>
#include <SDL_image.h>

#include <istream>
#include <map>
#include <vector>

#include "Global.h"
#include "Texture.h"

using namespace std;

class TextureManager
{
private:
	map<string, Texture*> textures;
	SDL_Renderer* renderer;

public:
	TextureManager();
	~TextureManager();

	bool addTexture(string name, string path, bool fromContent = true);
	bool addTexture(string name, Texture* texture);
	Texture* getTexture(string name);
	void removeTexture(string name);

	map<string, Texture*>* getTextureMap();
	void setRenderer(SDL_Renderer* renderer);
	int getCount();
};

//global texture manager
extern TextureManager gTextureManager;

