#include "PrimitiveDrawing.h"

void setPixel(SDL_Renderer * renderer, int x, int y, Uint8 r, Uint8 g, Uint8 b, Uint8 a)
{
	SDL_SetRenderDrawColor(renderer, r, g, b, a);
	SDL_RenderDrawPoint(renderer, x, y);
}

void drawCircle(SDL_Renderer * renderer, int n_cx, int n_cy, int radius, Uint8 r, Uint8 g, Uint8 b, Uint8 a)
{
	// if the first pixel in the screen is represented by (0,0) (which is in sdl)
	// remember that the beginning of the circle is not in the middle of the pixel
	// but to the left-top from it:

	double error = (double)-radius;
	double x = (double)radius - 0.5;
	double y = (double)0.5;
	double cx = n_cx - 0.5;
	double cy = n_cy - 0.5;

	while (x >= y)
	{
		setPixel(renderer, (int)(cx + x), (int)(cy + y), r, g, b, a);
		setPixel(renderer, (int)(cx + y), (int)(cy + x), r, g, b, a);

		if (x != 0)
		{
			setPixel(renderer, (int)(cx - x), (int)(cy + y), r, g, b, a);
			setPixel(renderer, (int)(cx + y), (int)(cy - x), r, g, b, a);
		}

		if (y != 0)
		{
			setPixel(renderer, (int)(cx + x), (int)(cy - y), r, g, b, a);
			setPixel(renderer, (int)(cx - y), (int)(cy + x), r, g, b, a);
		}

		if (x != 0 && y != 0)
		{
			setPixel(renderer, (int)(cx - x), (int)(cy - y), r, g, b, a);
			setPixel(renderer, (int)(cx - y), (int)(cy - x), r, g, b, a);
		}

		error += y;
		++y;
		error += y;

		if (error >= 0)
		{
			--x;
			error -= x;
			error -= x;
		}
	}
}

void fillCircle(SDL_Renderer * renderer, int cx, int cy, int radius, Uint8 r, Uint8 g, Uint8 b, Uint8 a)
{
	// Note that there is more to altering the bitrate of this 
	// method than just changing this value.  See how pixels are
	// altered at the following web page for tips:
	//   http://www.libsdl.org/intro.en/usingvideo.html
	static const int BPP = 4;

	//double ra = (double)radius;

	for (double dy = 1; dy <= radius; dy += 1.0)
	{
		// This loop is unrolled a bit, only iterating through half of the
		// height of the circle.  The result is used to draw a scan line and
		// its mirror image below it.

		// The following formula has been simplified from our original.  We
		// are using half of the width of the circle because we are provided
		// with a center and we need left/right coordinates.

		double dx = floor(sqrt((2.0 * radius * dy) - (dy * dy)));
		int x = cx - dx;
		SDL_SetRenderDrawColor(renderer, r, g, b, a);
		SDL_RenderDrawLine(renderer, cx - dx, cy + dy - radius, cx + dx, cy + dy - radius);
		SDL_RenderDrawLine(renderer, cx - dx, cy - dy + radius, cx + dx, cy - dy + radius);
	}
}

void drawRectangle(SDL_Renderer * renderer, int x, int y, int width, int height, Uint8 r, Uint8 g, Uint8 b, Uint8 a)
{
	SDL_Rect rec;
	rec.x = x;
	rec.y = y;
	rec.w = width;
	rec.h = height;
	//SDL_SetRenderDrawColor(renderer, r, g, b, a);
	//SDL_RenderDrawRect(renderer, &rec);
	rectBatch.push_back(rec);
}

void fillRectangle(SDL_Renderer * renderer, int x, int y, int width, int height, Uint8 r, Uint8 g, Uint8 b, Uint8 a)
{
	SDL_Rect rec;
	rec.x = x;
	rec.y = y;
	rec.w = width;
	rec.h = height;
	//SDL_SetRenderDrawColor(renderer, r, g, b, a);
	//SDL_RenderFillRect(renderer, &rec);

	rectBatchFill.push_back(rec);
}

void drawLine(SDL_Renderer * renderer, int x1, int y1, int x2, int y2, Uint8 r, Uint8 g, Uint8 b, Uint8 a)
{
	SDL_SetRenderDrawColor(renderer, r, g, b, a);
	SDL_RenderDrawLine(renderer, x1, y1, x2, y2);
}

void drawPoint(SDL_Renderer * renderer, int x, int y, Uint8 r, Uint8 g, Uint8 b, Uint8 a)
{
	SDL_Point point;
	SDL_Color col;
	col.r = r;
	col.g = g;
	col.b = b;
	col.a = a;
	//pointsMap[col].push_back(point);
	pointsMap.push_back(point);

	//SDL_SetRenderDrawColor(renderer, r, g, b, a);
	//SDL_RenderDrawPoint(renderer, x, y);
}

void drawPoint(SDL_Renderer * renderer, Vector2D position, Color color)
{
	SDL_Point point;
	point.x = position.x;
	point.y = position.y;
	SDL_Color col;
	col.r = color.r;
	col.g = color.g;
	col.b = color.b;
	col.a = color.a;
	//pointsMap[col].push_back(point);
	pointsMap.push_back(point);

	//SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
	//SDL_RenderDrawPoint(renderer, position.x, position.y);
}

void drawPrimitives(SDL_Renderer * renderer, Camera* camera)
{	
	int x = 0, y = 0;
		
	if (camera != nullptr)
	{
		x = camera->position.x;
		y = camera->position.y;
	}

	if (rectBatch.size() != 0)
	{
		for (int i = 0; i < rectBatch.size(); i++)
		{
			rectBatch[i].x += x;
			rectBatch[i].y += y;
		}
		const SDL_Rect *rec = &rectBatch[0];
		SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0xFF, 0xFF);
		SDL_RenderDrawRects(renderer, rec, rectBatch.size());
		rectBatch.clear();
	}
	
	if (rectBatchFill.size() != 0) 
	{
		for (int i = 0; i < rectBatchFill.size(); i++)
		{
			rectBatchFill[i].x += x;
			rectBatchFill[i].y += y;
		}
		const SDL_Rect *recFill = &rectBatchFill[0];
		SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0xFF, 0xFF);
		SDL_RenderDrawRects(renderer, recFill, rectBatchFill.size());
		rectBatchFill.clear();
	}
	if (pointsMap.size() != 0) {
		for (int i = 0; i < pointsMap.size(); i++)
		{
			pointsMap[i].x += x;
			pointsMap[i].y += y;
		}
		const SDL_Point *vec = &pointsMap[0];
		SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0xFF, 0xFF);
		SDL_RenderDrawPoints(renderer, vec, pointsMap.size());
		pointsMap.clear();
	}
}

