#pragma once
#include <SDL.h>

#include "Camera.h"

#include "ISystem.h"
#include "CompIncludes.h"

#include "EventTypes.h"
//#include "SpriteManager.h"

#include "Grid.h"

class SSpriteRenderer :public ISystem
{
public:
	SSpriteRenderer(SDL_Renderer* renderer, Camera* camera = NULL);
	~SSpriteRenderer();

	void init() override;
	void update(double deltaTime) override;

	void onCollisionEvent(CollisionEvent *collision);
	struct Info {
		Info(Transform *t, SpriteRenderer* s) { this->t = t; this->s = s; };
		Transform *t;
		SpriteRenderer* s;
	};
private:
	SDL_Renderer * renderer;
	Camera* camera;
};

