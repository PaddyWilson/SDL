#pragma once

#include <algorithm>

#include "Global.h"

#include "Camera.h"

#include "ISystem.h"
#include "EntityHandle.h"

#include "CompIncludes.h"
#include "Rectangle.h"
#include "Quadtree.h"

#include "Grid.h"

#include "EventTypes.h"

#include "PrimitiveDrawing.h"

class SCollision : public ISystem
{
public:
	SCollision(Camera* cam = new Camera());
	~SCollision();

	void init() override;
	void update(double deltaTime) override;

	void addEntity(Entity e) override;
	void removeEntity(Entity e) override;

	//used to hold info for later use
	struct CollisionInfo {
		CollisionInfo() {};
		CollisionInfo(EntityHandle e1, BaseCollider* col1, Transform *t1, EntityHandle e2, BaseCollider *col2, Transform *t2) :
			e1(e1), col1(col1), t1(t1), e2(e2), col2(col2), t2(t2)
		{};

		EntityHandle e1;
		Transform *t1;
		BaseCollider *col1;

		EntityHandle e2;
		Transform *t2;
		BaseCollider *col2;
	};

	struct EntityInfo
	{
		EntityHandle entity;
		BaseCollider *collider;
		Transform *transform;

		bool operator==(const EntityInfo &info) const {
			return entity.entity.id == info.entity.entity.id;
		}
	};

private:
	Camera * camera;
	Grid<EntityInfo> grid;
	Grid<EntityInfo> staticGrid;

	vector<Entity> staticEntities;

	map<int, vector<int>> allReadyHit;

	vector<EntityInfo>hits;
	vector<EntityInfo>staticHits;

	void debugDraw(BaseCollider *collider, Transform *trans);
};
