#pragma once

#include "ISystem.h"
#include "CompIncludes.h"

class SMovement: public ISystem
{
public:
	SMovement();
	~SMovement();

	void init() override;
	void update(double dt) override;
};