#pragma once

#include "Component.h"
//typedef int EntityID;

class Entity
{
public:
	int id;

private:
	static int nextId;

public:
	Entity();
	Entity(int id);
	~Entity();

	bool operator==(const Entity &e) const {
		return id == e.id;
	}
};
