#pragma once
#include "Vector2D.h"

class Camera
{
public:
	Camera(Vector2D pos = Vector2D(), Vector2D screenSize = Vector2D(1280, 720));
	~Camera();

	Vector2D position;
	Vector2D screenSize;
	//float worldToPix = 64;

	void moveTo(Vector2D move) { position = move; };
	void move(Vector2D move) { position += move; };

	inline Vector2D getPostion() {
		return position;
	};
};


