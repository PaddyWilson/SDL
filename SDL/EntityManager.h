#pragma once

#include <vector>
#include <queue>

#include "Entity.h"

using namespace std;

class EntityManager
{
private:
	vector<Entity> entities;
	//map<int, bitset<AMOUNT_OF_COMPONENTS>> entityBits;

	//queue<Entity> addQueue;
	//queue<Entity> removeQueue;

public:
	Entity createEntity();

	Entity add(Entity e);
	Entity get(int id);
	Entity getIndex(int index);
	void remove(Entity e);

	int size();
};