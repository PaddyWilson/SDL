#pragma once

#include "imgui/imgui.h"
#include "imgui/imgui_impl_sdl_gl3.h"
#include <stdio.h>

#include <GL/gl3w.h>    // This example is using gl3w to access OpenGL functions (because it is small). You may use glew/glad/glLoadGen/etc. whatever already works for you.
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <SDL_mixer.h>
#include <fstream>

#include "Helper.h"
#include "Global.h"

//declare for global renderer in Globals.h
SDL_Renderer* gRenderer;
float gDeltaTime;

#include "TextureManager.h"
#include "SpriteManager.h"
#include "AudioManager.h"
#include "Font.h"

#include "Window.h"
#include "Viewport.h"

#include "World.h"

#include "CompIncludes.h"
#include "ICommandIncludes.h"
#include "SystemIncludes.h"

#include "InputHandler.h"
#include "Camera.h"
#include "Animation.h"
#include "Timer.h"
#include "ChronoTimer.h"

//Screen dimension constants
const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 720;

const bool VSYNC = false;
const bool ENABLE_IMGUI = true;

bool setupSDL();
void closeSDL();

bool setupIMGUI();
void renderUMGUI(SDL_Window* window, bool quit);
void closeIMGUI();

void loadAllTextures();
void loadAllAudio();
void createSprites();

void createPrefabs();

//windows
Window mainWindow("SDL2", 410, SDL_WINDOWPOS_CENTERED, 1280, 720, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
Window imguiWindow("ImGui", 0, 30, 400, 720, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);

//IMGUI Window and opengl context
//SDL_Window *im_window;
SDL_DisplayMode current;
SDL_GLContext im_context;

//main window and renederer
//SDL_Window *window;
SDL_Renderer* renderer;

World world;

int main(int, char**)
{
	// Setup SDL
	if (!setupSDL()) return-1;
	//SDL_ShowCursor(SDL_DISABLE);
	// Setup Dear ImGui binding
	if (ENABLE_IMGUI)
		if (!setupIMGUI()) return-2;

	mainWindow.focus();

	//sets a global renderer variable
	gRenderer = renderer;//do something different
	//set texture managers renderer
	gTextureManager.setRenderer(renderer);

	//load font
	Font font;
	font.loadFont(FONT_PATH + "Ubuntu-R.ttf");
	//create a static texture from a font
	gTextureManager.addTexture("font test", font.createTexture(renderer, "Font Test", 40));

	//load a texture
	//gTextureManager.addTexture("crate1", "crate_01.png");
	loadAllTextures();
	cout << "Loaded " << gTextureManager.getCount() << " textures" << endl;

	Mix_AllocateChannels(64);
	gAudioMgr.load("beat", "beat.wav", true);
	gAudioMgr.load("shax1", "charge defused, relax, nothing explodes this round.mp3");
	gAudioMgr.load("shax2", "jingles_STEEL00.ogg");
	loadAllAudio();
	cout << "Loaded " << gAudioMgr.getCount() << " audio files" << endl;

	//create sprites
	createSprites();
	cout << "Created " << gSpriteManager.getCount() << " sprites" << endl;

	//setup event system;
	//EventSystem eventSystem;
	//world = World(&eventSystem);

	//setup camera
	Camera camera(Vector2D(0, 0));

	//add systems
	world.addSystem(new SMovement());
	world.addSystem(new SCollision(&camera));
	world.addSystem(new FollowSystem());
	world.addSystem(new AnimationSystem());
	world.addSystem(new SSpriteRenderer(renderer, &camera));

	//call world init after adding systems
	world.init();

	createPrefabs();

	//create entities
	EntityHandle entity = world.createEntity();

	//test prefabs entity
	//entity = world.createPrefab("test", 20.0f, 20.0f);
	//entity = world.createPrefab("test", 200.0f, 20.0f);

	for (size_t i = 10; i < 50; i++)
	{
		int x = (16 * i) + i;
		world.createPrefab("boxCollider", x, 720 / 2 - 20);
		world.createPrefab("boxCollider", x, 720 / 2 - 20 - (17 * 1));
		world.createPrefab("boxCollider", x, 720 / 2 - 20 - (17 * 2));
	}

	//mouse
	entity = world.createEntity();
	Transform* mouseTrans = entity.addComponent(Transform());
	entity.addComponent(Movement());
	//entity.addComponent(BaseCollider(ColliderType::PointType));
	entity.addComponent(BaseCollider(ColliderType::BoxType, Vector2D(64, 64)));
	//entity.addComponent(BaseCollider(ColliderType::CircleType, 32));
	//entity.addComponent(BaseCollider(ColliderType::LineType, Vector2D(0, 128), Vector2D(128, 0)));
	//entity.addComponent(BaseCollider(ColliderType::LineType, Vector2D(0, 0), Vector2D(20, 20)));
	entity.addComponent(SpriteRenderer("genericItem_color_086"));

	//change sprites
	static int spriteIndex = 0;
	entity = world.createEntity();
	entity.addTag("spriteTest");
	entity.addComponent(Transform(20, 20));
	SpriteRenderer* spriteChange = entity.addComponent(SpriteRenderer(gSpriteManager.getSprite(gSpriteManager.getNames()->at(0))));

	entity = world.createEntity();
	entity.addComponent(Transform());
	Movement* move = entity.addComponent(Movement());
	move->velocity = Vector2D(1000, 0);
	//entity.addComponent(BaseCollider(ColliderType::PointType));
	//entity.addComponent(BaseCollider(ColliderType::BoxType, Vector2D(200, 200)));
	//entity.addComponent(BaseCollider(ColliderType::CircleType, 32));
	//entity.addComponent(BaseCollider(ColliderType::LineType, Vector2D(0, 128), Vector2D(128, 0)));
	entity.addComponent(BaseCollider(ColliderType::LineType, Vector2D(0, 0), Vector2D(20, 20)));
	entity.addComponent(SpriteRenderer("crosshair005", Vector2D(-64, -64)));
	entity.addGroup("Follower");

	//follower
	EntityHandle entity2 = world.createPrefab("follower", 0, 0);
	entity2.getComponent<Follow>()->entityToFollow = entity.entity;
	entity2.getComponent<SpriteRenderer>()->sprite = gSpriteManager.getSprite("orange");

	//other followers
	for (size_t i = 0; i < 25; i++)
	{
		entity = world.createPrefab("follower", 0, 0);
		entity.getComponent<Follow>()->entityToFollow = entity2.entity;
		entity.getComponent<SpriteRenderer>()->sprite = gSpriteManager.getSprite("green");

		entity2 = world.createPrefab("follower", 0, 0);
		entity2.getComponent<Follow>()->entityToFollow = entity.entity;
		entity2.getComponent<SpriteRenderer>()->sprite = gSpriteManager.getSprite("orange");
	}

	//test colliders
	int pointCountx = 0;
	int pointCounty = 50;

	//for (int i = 0; i < 5000; i++)
	//{
	//	int x = 10 + (15 * pointCountx);
	//	int y = 10 + (15 * pointCounty);
	//	world.createPrefab("pointCollider", x, y);
	//	pointCountx++;
	//	if (x > 1270 * 3)
	//	{
	//		pointCountx = 0;
	//		pointCounty++;
	//	}
	//	if (i % 10000 == 0)
	//	{
	//		cout << "Count " << i << endl;
	//	}
	//}

	//entity = world.createEntity();
	//entity.addComponent(Transform(50, 400));
	//entity.addComponent(BaseCollider(ColliderType::PointType));
	//entity.addComponent(SpriteRenderer("1x1"));

	//entity = world.createEntity();
	//entity.addComponent(Transform(200, 400));
	//entity.addComponent(BaseCollider(ColliderType::BoxType, Vector2D(128, 128)));
	//entity.addComponent(SpriteRenderer("128x128"));

	//entity = world.createEntity();
	//entity.addComponent(Transform(500, 400));
	//entity.addComponent(BaseCollider(ColliderType::CircleType, 64, Vector2D(64, 64)));
	//entity.addComponent(SpriteRenderer("circle128"));

	//entity = world.createEntity();
	//entity.addComponent(Transform(800, 400));
	//entity.addComponent(BaseCollider(ColliderType::LineType, Vector2D(0, 0), Vector2D(128, 128)));
	////entity.addComponent(SpriteRenderer("crate_01"));

	//entity = world.createEntity();
	//entity.addComponent(Transform(800, 200));
	//entity.addComponent(BaseCollider(ColliderType::LineType, Vector2D(0, 128), Vector2D(128, 0)));
	//entity.addComponent(SpriteRenderer("crate_01"));

	Animation animate(true);
	animate.addFrame(gSpriteManager.getSprite("crate_01"), 500);
	animate.addFrame(gSpriteManager.getSprite("crate_02"), 500);
	animate.addFrame(gSpriteManager.getSprite("crate_03"), 500);
	animate.addFrame(gSpriteManager.getSprite("crate_04"), 500);
	animate.start();

	Animation animate2(true);
	animate2.addFrame(gSpriteManager.getSprite("player_01"), 500);
	animate2.addFrame(gSpriteManager.getSprite("player_02"), 500);
	animate2.addFrame(gSpriteManager.getSprite("player_03"), 500);
	animate2.addFrame(gSpriteManager.getSprite("player_04"), 500);
	animate2.addFrame(gSpriteManager.getSprite("player_05"), 500);
	animate2.addFrame(gSpriteManager.getSprite("player_06"), 500);

	Animation things(true);
	things.addFrame(gSpriteManager.getSprite("playerBlue_walk1"), 200);
	things.addFrame(gSpriteManager.getSprite("playerBlue_walk2"), 200);
	things.addFrame(gSpriteManager.getSprite("playerBlue_walk3"), 200);
	things.addFrame(gSpriteManager.getSprite("playerBlue_walk2"), 200);
	//things.addFrame(gSpriteManager.getSprite("playerBlue_walk5"), 500);
	things.start();

	entity = world.createEntity();
	//mouseTrans = entity.addComponent(Transform());
	CAnimation*an = entity.addComponent(CAnimation());
	entity.addComponent(SpriteRenderer());
	an->animations["test"] = things;
	an->animations["player"] = things;
	an->currentAnimation = "test";

	//called so all the entities that are add before
	//the main loop get added to all systems
	world.update(0);

	cout << "EntityMgr Size " << world.entityManager.size() << endl;
	cout << "CompMgr Size " << world.componentCount<Transform>() << endl;
	cout << "TagMgr Size " << world.tagMgr.size() << endl;
	cout << "GroupMgr Size " << world.groupMgr.size() << endl;
	cout << "GroupMgr Size Follower" << world.groupMgr.size("Follower") << endl;
	world.groupMgr.get(entity.entity);
	InputHandler inputHandler;

	//map keys to commands
	//inputHandler.mapKey(SDL_SCANCODE_A, new MoveLeft(player), InputHandler::KeyState::Down);
	//inputHandler.mapKey(SDL_SCANCODE_D, new MoveRight(player), InputHandler::KeyState::Down);
	//inputHandler.mapKey(SDL_SCANCODE_W, new MoveUp(player), InputHandler::KeyState::Down);
	//inputHandler.mapKey(SDL_SCANCODE_S, new MoveDown(player), InputHandler::KeyState::Down);

	//camera movement
	inputHandler.mapKey(SDL_SCANCODE_UP, new MoveCamUp(&camera), InputHandler::KeyState::Down);
	inputHandler.mapKey(SDL_SCANCODE_DOWN, new MoveCamDown(&camera), InputHandler::KeyState::Down);
	inputHandler.mapKey(SDL_SCANCODE_LEFT, new MoveCamLeft(&camera), InputHandler::KeyState::Down);
	inputHandler.mapKey(SDL_SCANCODE_RIGHT, new MoveCamRight(&camera), InputHandler::KeyState::Down);

	//zoom
	static float scale = 1.0f;
	inputHandler.mapKey(SDL_SCANCODE_KP_PLUS, []() { scale += 0.1; }, InputHandler::KeyState::Pressed);
	inputHandler.mapKey(SDL_SCANCODE_KP_MINUS, []() { scale -= 0.1; }, InputHandler::KeyState::Pressed);

	//audio test
	inputHandler.mapKey(SDL_SCANCODE_1, []() { gAudioMgr.play("shax1"); }, InputHandler::KeyState::Pressed);
	inputHandler.mapKey(SDL_SCANCODE_2, []() { gAudioMgr.play("shax2"); }, InputHandler::KeyState::Pressed);
	inputHandler.mapKey(SDL_SCANCODE_9,
		[]() { if (Mix_PlayingMusic() == 0)	gAudioMgr.play("beat");
		else { (Mix_PausedMusic() == 1) ? gAudioMgr.resume() : gAudioMgr.pause(); }},
		InputHandler::KeyState::Pressed);
	inputHandler.mapKey(SDL_SCANCODE_0, []() { gAudioMgr.stop(); }, InputHandler::KeyState::Pressed);

	//sprite changer
	static string spriteName;
	inputHandler.mapKey(SDL_SCANCODE_K, []() {
		spriteIndex++;
		if (spriteIndex >= gSpriteManager.getNames()->size())
			spriteIndex = 0;
		spriteName = gSpriteManager.getNames()->at(spriteIndex);
		world.getTag("spriteTest").getComponent<SpriteRenderer>()->sprite = gSpriteManager.getSprite(spriteName);
		cout << spriteName << " " << spriteIndex << endl;
	}, InputHandler::KeyState::Pressed);

	//frame info
	Uint64 now = SDL_GetPerformanceCounter();
	Uint64 last = 0;
	double deltaTime = 0;
	double deltaTimeMS = 0;

	// Main loop
	int frameCount = 0;
	bool quit = false;
	bool pause = false;
	SDL_Event sdlEvent;
	while (!quit)
	{
		//calculate delta time
		last = now;
		now = SDL_GetPerformanceCounter();
		deltaTime = (double)((now - last) * 1000 / SDL_GetPerformanceFrequency());
		deltaTimeMS = deltaTime;
		deltaTime *= 0.001;//convert it to seconds
		gDeltaTime = deltaTime;//set the global deltaTime var
		//SDL_SetWindowTitle(window, to_string(deltaTime).c_str());		

		//Handle events on queue
		int x, y;//mouse postion
		while (SDL_PollEvent(&sdlEvent))
		{
			mainWindow.handleEvents(sdlEvent);

			if (ENABLE_IMGUI)
			{
				imguiWindow.handleEvents(sdlEvent);
				ImGui_ImplSdlGL3_ProcessEvent(&sdlEvent);
			}

			SDL_GetMouseState(&x, &y);
			x /= scale;
			y /= scale;
			x -= camera.position.x;
			y -= camera.position.y;

			if (mainWindow.hasMouseFocus()) {
				mouseTrans->position.x = (float)x;// / scale;
				mouseTrans->position.y = (float)y;// / scale;
			}

			if (sdlEvent.type == SDL_WINDOWEVENT) {
				switch (sdlEvent.window.event)
				{
				case SDL_WINDOWEVENT_CLOSE:
					quit = true;
				default:
					break;
				}
			}

			//mouse click
			if (sdlEvent.type == SDL_MOUSEBUTTONDOWN && sdlEvent.window.windowID == mainWindow.getWindowID())
			{
				if (sdlEvent.button.button == SDL_BUTTON_RIGHT)
				{
					world.createPrefab("pointCollider", (float)x, (float)y);
				}
			}

			//keyboard
			if (sdlEvent.type == SDL_KEYDOWN && sdlEvent.window.windowID == mainWindow.getWindowID())
			{
				switch (sdlEvent.key.keysym.sym)
				{
				case SDLK_SPACE:
					pause ? pause = false : pause = true;
					/*if (pause)
						pause = false;
					else
						pause = true;*/
					break;
				case SDLK_q:
					an->animations[an->currentAnimation].start();
					animate.start();
					break;
				case SDLK_w:
					an->animations[an->currentAnimation].stop();
					animate.stop();
					break;
				case SDLK_e:
					an->animations[an->currentAnimation].pause();
					animate.pause();
					break;
				case SDLK_r:
					an->animations[an->currentAnimation].restart();
					animate.restart();
					break;
				case SDLK_t:
					if (an->currentAnimation == "test")
						an->currentAnimation = "player";
					else
						an->currentAnimation = "test";
					break;

				case SDLK_d:
					if (world.entityManager.size() > 0)
					{
						entity = EntityHandle();
						entity.entity = world.entityManager.getIndex(0);
						entity.world = &world;
						entity.destroy();
					}
					break;
				case SDLK_a:
					entity = world.createPrefab("pointCollider", 10 + (15 * pointCountx), 10 + (15 * pointCounty));
					//entity = world.createEntity();
					//entity.addComponent(Transform(10 + (3 * pointCountx), 10 + (3 * pointCounty)));
					//entity.addComponent(BaseCollider(ColliderType::PointType));
					pointCountx++;
					if (10 + (3 * pointCountx) > 1270)
					{
						pointCountx = 0;
						pointCounty++;
					}
					cout << "EntityMgr Size " << world.entityManager.size() << endl;
					break;
				case SDLK_ESCAPE:
					quit = true;
					break;
				default:
					break;
				}
			}
		}
		if (mainWindow.hasKeyboardFocus())
			inputHandler.update();
		//SDL_RenderSetLogicalSize(renderer, 1280, 720);

		//Clear screen
		SDL_SetRenderDrawColor(renderer, 127, 0xFF, 0xFF, 0xFF);
		SDL_RenderClear(renderer);

		//process inputs
		inputHandler.processCommands();

		//AnimationFrame * frame = things.getNextFrame();
		//frame->sprite->render(renderer, Vector2D(400, 400));

		if (!pause) {
			world.update(deltaTime);
		}
		//	//scale
		//scale = 0.5;
		SDL_RenderSetScale(renderer, scale, scale);

		drawPrimitives(renderer, &camera);

		//font test
		//"Application average " + 1000.0f / ImGui::GetIO().Framerate +" ms/frame ("+ImGui::GetIO().Framerate+" FPS)",
		//font.write(renderer, ("DT:" + to_string(1000.0f / ImGui::GetIO().Framerate) + " FPS:" + to_string(ImGui::GetIO().Framerate)), 0, 200, 50);
		SDL_Color fontColor;
		fontColor.r = 0;
		fontColor.g = 0;
		fontColor.b = 0;
		fontColor.a = 255;
		//font.write(renderer, "DT:" + to_string((int)deltaTimeMS), 0, 200, 50, fontColor);
		font.write(renderer, "Sprite:" + spriteName, 0, 720 - 60, 50);
		//gTextureManager.getTexture("font test")->render(renderer, 0, 400);

		SDL_RenderPresent(renderer);

		//IMGUI WINDOW RENDER
		if (ENABLE_IMGUI)// enabled
			renderUMGUI(imguiWindow.getWindow(), quit);

		font.clearTextures();
		frameCount++;
	}

	// Cleanup
	if (ENABLE_IMGUI)
		closeIMGUI();

	font.unloadFont();

	closeSDL();

	cout << "Close Window" << endl;
	return 0;
}

bool setupSDL() {

	// Setup SDL
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_AUDIO) != 0)
	{
		printf("Error: %s\n", SDL_GetError());
		return false;
	}

	//Set texture filtering to linear
	if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "0"))
	{
		printf("Warning: Nearest texture filtering not enabled!");
		return false;
	}

	//mainWindow = Window("SDL2", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1280, 720, SDL_WINDOW_RESIZABLE);
	mainWindow.init();
	//window = mainWindow.getWidow();//SDL_CreateWindow("SDL2", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1280, 720, SDL_WINDOW_RESIZABLE);

	if (VSYNC)
		renderer = SDL_CreateRenderer(mainWindow.getWindow(), -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	else
		renderer = SDL_CreateRenderer(mainWindow.getWindow(), -1, SDL_RENDERER_ACCELERATED);

	//init subsystems
	//Initialize SDL_image for png loading
	int imgFlags = IMG_INIT_PNG;
	if (!(IMG_Init(imgFlags) & imgFlags))
	{
		printf("SDL_image could not initialize! SDL_mage Error: %s\n", IMG_GetError());
		return false;
	}

	//Initialize SDL_ttf fonts
	if (TTF_Init() == -1)
	{
		printf("SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError());
		return false;
	}

	//Initialize SDL_mixer
	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
	{
		printf("SDL_mixer could not initialize! SDL_mixer Error: %s\n", Mix_GetError());
		return false;
	}

	return true;
}

void closeSDL()
{
	//SDL_GL_DeleteContext(gl_context);	
	mainWindow.close();//SDL_DestroyWindow(mainWindow.getWindow());

	//quit SDL subsystems
	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
}

bool setupIMGUI()
{
	// Setup window
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
	SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
	SDL_GetCurrentDisplayMode(0, &current);

	//imguiWindow = Window("ImGui", 0, 30, 400, 700, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
	imguiWindow.init();
	//im_window = imguiWindow.getWidow();//SDL_CreateWindow("ImGui", 0, 30, 400, 700, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);

	im_context = SDL_GL_CreateContext(imguiWindow.getWindow());

	if (VSYNC)
		SDL_GL_SetSwapInterval(0); // Enable vsync

	gl3wInit();

	// Setup Dear ImGui binding
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO(); (void)io;
	//io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;  // Enable Keyboard Controls
	ImGui_ImplSdlGL3_Init(imguiWindow.getWindow());

	// Setup style
	//ImGui::StyleColorsLight();
	//ImGui::StyleColorsClassic();
	ImGui::StyleColorsDark();

	return true;
}

void renderUMGUI(SDL_Window * window, bool quit)
{
	ImGui_ImplSdlGL3_NewFrame(window);

	// Menu
	if (ImGui::BeginMainMenuBar()) {
		if (ImGui::BeginMenu("File")) {
			if (ImGui::MenuItem("Exit"))
				quit = false;
			ImGui::EndMenu();
		}
		if (ImGui::BeginMenu("Edit")) {
			ImGui::EndMenu();
		}
		if (ImGui::BeginMenu("Window")) {
			ImGui::EndMenu();
		}
		if (ImGui::MenuItem("VIP"))
			if (ImGui::BeginMenu("Help")) {
				ImGui::EndMenu();
			}
		ImGui::EndMainMenuBar();
	}

	// 1. Show a simple window.
	// Tip: if we don't call ImGui::Begin()/ImGui::End() the widgets automatically appears in a window called "Debug".
	{
		ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
	}

	{
		ImGui::Begin("Count Window");
		ImGui::Text("Entity Count %i", world.entityManager.size());
		ImGui::Separator();
		//ImGui::Columns(2, NULL, true);
		for (size_t i = 0; i < AMOUNT_OF_COMPONENTS; i++)
		{
			if (world.getCompNames()[i] != "") {
				
				ImGui::Text("%s", world.getCompNames()[i].erase(0, 7).c_str());
				//ImGui::NextColumn();
				ImGui::SameLine();
				ImGui::SetCursorPosX(ImGui::GetWindowSize().x - ImGui::CalcTextSize(to_string(world.componentCount(i)).c_str()).x - 8);
				ImGui::Text("%i", world.componentCount(i));
				//ImGui::NextColumn();
			}
		}
		ImGui::End();
	}

	{
		ImGui::Begin("Tags Window");
		ImGui::Text("Tags Count:%i", world.tagMgr.size());
		ImGui::Separator();
		auto items = world.tagMgr.getTagList();
		for (size_t i = 0; i < items.size(); i++) {
			ImGui::Text(items[i].c_str());
		}
		ImGui::End();
	}

	{
		ImGui::Begin("Groups Window");
		ImGui::Text("Groups Count:%i", world.groupMgr.size());
		ImGui::Separator();
		auto items = world.groupMgr.getGroupList();
		for (size_t i = 0; i < items.size(); i++) {
			ImGui::Text("%s", items[i].c_str());
			ImGui::SameLine();
			ImGui::SetCursorPosX(ImGui::GetWindowSize().x - ImGui::CalcTextSize(to_string(world.getGroup(items[i])->size()).c_str()).x - 8);
			ImGui::Text(to_string(world.getGroup(items[i])->size()).c_str());
		}
		ImGui::End();
	}

	{
		ImGui::Begin("Sprite Window");
		static int combo = 0;
		vector<const char*> cstrings{};
		for (size_t i = 0; i < gSpriteManager.getNames()->size(); i++) {
			cstrings.push_back(gSpriteManager.getNames()->at(i).c_str());
		}
		if (ImGui::Combo("Change sprite", &combo, cstrings.data(), gSpriteManager.getNames()->size())) {
			world.getTag("spriteTest").getComponent<SpriteRenderer>()->sprite = gSpriteManager.getSprite(gSpriteManager.getNames()->at(combo));
		}
		ImGui::End();
	}

	// Rendering
	ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
	glViewport(0, 0, (int)ImGui::GetIO().DisplaySize.x, (int)ImGui::GetIO().DisplaySize.y);
	glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
	glClear(GL_COLOR_BUFFER_BIT);
	ImGui::Render();
	ImGui_ImplSdlGL3_RenderDrawData(ImGui::GetDrawData());
	SDL_GL_SwapWindow(window);
}

void closeIMGUI()
{
	ImGui_ImplSdlGL3_Shutdown();
	ImGui::DestroyContext();

	SDL_GL_DeleteContext(im_context);
	imguiWindow.close();//SDL_DestroyWindow(im_window);
}

void loadAllTextures()
{
	//file read test
	std::string path = TEXTURE_PATH;
	for (auto & p : getFilesInFolder(path))
	{
		string fileName = "";
		string filePath = p;
		string ext;

		std::vector<std::string> seglist = splitString(filePath, '/');
		fileName = seglist[seglist.size() - 1];
		ext = splitString(fileName, '.')[1];
		fileName = splitString(fileName, '.')[0];

		if (ext != "txt")
			gTextureManager.addTexture(fileName, filePath, false);
	}
}

void loadAllAudio()
{
	//file read test
	std::string path = AUDIO_PATH;
	for (auto & p : getFilesInFolder(path))
	{
		string fileName = "";
		string filePath = p;
		string ext;

		std::vector<std::string> seglist = splitString(filePath, '/');
		fileName = seglist[seglist.size() - 1];
		ext = splitString(fileName, '.')[1];
		fileName = splitString(fileName, '.')[0];

		if (ext != "txt")
			gAudioMgr.load(fileName, filePath, false, false);
	}
}

void createSprites()
{
	for (auto t : *gTextureManager.getTextureMap())
	{
		gSpriteManager.create(t.first, t.first);
	}

	//load all sprite sheets;
	std::string path = TEXTURE_PATH;
	for (auto & p : getFilesInFolder(path))
	{
		string fileName = "";
		string filePath = p;
		string ext = "";

		std::vector<std::string> seglist = splitString(filePath, '/');
		fileName = seglist[seglist.size() - 1];
		ext = splitString(fileName, '.')[1];
		fileName = splitString(fileName, '.')[0];

		//has sprite sheet extention
		if (ext == "txt")
		{
			//read file
			vector<string> strings;
			ifstream infile;
			infile.open(filePath);
			string temp;
			if (infile.is_open()) {
				while (!infile.eof()) {
					getline(infile, temp);
					strings.push_back(temp);
				}
			}
			infile.close();

			//
			for (auto item : strings)
			{
				auto items = splitString(item, ' ');

				if (items.size() == 5)
				{
					gSpriteManager.create(items[0], fileName, stoi(items[1]), stoi(items[2]), stoi(items[3]), stoi(items[4]));
				}
			}
		}
	}
}//end createSprites()

void createPrefabs()
{
	//add prefabs
	world.addPrefab("test", [](float x, float y) {
		EntityHandle entity = world.createEntity();
		entity.addGroup("test");
		entity.addComponent(Transform(x, y));
		entity.addComponent(Movement());
		entity.addComponent(SpriteRenderer("crate_09"));
		return entity;
	});

	world.addPrefab("mouse", [](float x, float y) {
		EntityHandle entity = world.createEntity();
		entity.addTag("mouse");
		entity.addComponent(Transform(x, y));
		entity.addComponent(Movement());
		entity.addComponent(SpriteRenderer("crate_10"));
		return entity;
	});

	world.addPrefab("follower", [](float x, float y) {
		float radius = 8;
		float followSpeed = 250 * 2;
		EntityHandle entity2 = world.createEntity();
		entity2.addGroup("Follower");
		entity2.addComponent(Transform(x, y));
		entity2.addComponent(Movement());
		Follow* f = entity2.addComponent(Follow());
		f->radiusToFollow = radius;
		f->followSpeed = followSpeed;
		entity2.addComponent(SpriteRenderer("orange"));
		return entity2;
	});

	world.addPrefab("pointCollider", [](float x, float y) {
		EntityHandle entity = world.createEntity();
		entity.addGroup("pointCollider");
		entity.addComponent(Transform(x, y));
		entity.addComponent(BaseCollider(ColliderType::PointType));
		SpriteRenderer *r = entity.addComponent(SpriteRenderer("16x16"));
		//r->color = Color(0, 254, 254, 254);
		return entity;
	});

	world.addPrefab("boxCollider", [](float x, float y) {
		EntityHandle entity = world.createEntity();
		entity.addGroup("pointCollider");
		entity.addComponent(Transform(x, y));
		SpriteRenderer *r = entity.addComponent(SpriteRenderer("16x16"));
		entity.addComponent(BaseCollider(ColliderType::BoxType, r->sprite->size().convert<float>()));
		return entity;
	});
}//end createPrefabs()
