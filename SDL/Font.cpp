#include "Font.h"

void Font::loadFont(std::string file)
{
	fonts[20] = TTF_OpenFont(file.c_str(), 20);
	location = file;
}

void Font::unloadFont()
{
	std::map<int, TTF_Font*>::iterator it;
	for (it = fonts.begin(); it != fonts.end(); ++it)
	{
		if (it->second != nullptr)
			TTF_CloseFont(it->second);
	}
	fonts.clear();
	clearTextures();
}

void Font::fontSize(int size)
{
	if (fonts[size] == nullptr)
		fonts[size] = TTF_OpenFont(location.c_str(), size);
}

SDL_Texture * Font::write(SDL_Renderer* renderer, std::string text, int x, int y, int size, SDL_Color color)
{
	fontSize(size);//create a font of this size

	SDL_Surface* textSurface = TTF_RenderText_Solid(fonts[size], text.c_str(), color);
	SDL_Texture* texture = NULL;
	int w, h;

	if (textSurface == NULL)
	{
		printf("Unable to render text surface! SDL_ttf Error: %s\n", TTF_GetError());
	}
	else
	{
		//Create texture from surface pixels
		texture = SDL_CreateTextureFromSurface(renderer, textSurface);
		if (texture == NULL)
		{
			printf("Unable to create texture from rendered text! SDL Error: %s\n", SDL_GetError());
		}
		else
		{
			//Get image dimensions
			w = textSurface->w;
			h = textSurface->h;
		}

		//Get rid of old surface
		SDL_FreeSurface(textSurface);

		//Set rendering space and render to screen
		SDL_Rect renderQuad = { x, y, w, h };
		//render font
		SDL_RenderCopy(renderer, texture, NULL, &renderQuad);

		//add texture to list to be freed
		textures.push_back(texture);
	}
	return texture;
}

Texture * Font::createTexture(SDL_Renderer* renderer, std::string text, int size, SDL_Color color)
{
	fontSize(size);//create a font of this size

	SDL_Surface* textSurface = TTF_RenderText_Solid(fonts[size], text.c_str(), color);
	SDL_Texture* texture;
	int w, h;

	if (textSurface == NULL)
	{
		printf("Unable to render text surface! SDL_ttf Error: %s\n", TTF_GetError());
	}
	else
	{
		//Create texture from surface pixels
		texture = SDL_CreateTextureFromSurface(renderer, textSurface);
		if (texture == NULL)
		{
			printf("Unable to create texture from rendered text! SDL Error: %s\n", SDL_GetError());
		}
		else
		{
			//Get image dimensions
			w = textSurface->w;
			h = textSurface->h;
		}

		//Get rid of old surface
		SDL_FreeSurface(textSurface);

		Texture *tex = new Texture();
		tex->setTexture(texture, w, h);
		return tex;
	}
	return nullptr;
}

//call every frame to clean up created textures
void Font::clearTextures()
{
	for (size_t i = 0; i < textures.size(); i++)
	{
		if (textures[i] != NULL)
		{
			SDL_DestroyTexture(textures[i]);
		}
	}
	textures.clear();
}
