#pragma once
#include <SDL.h>
#include <SDL_image.h>
#include <vector>
#include <map>

#include "Vector2D.h"
#include "Camera.h"
#include "Color.h"

void setPixel(SDL_Renderer *renderer, int x, int y, Uint8 r, Uint8 g, Uint8 b, Uint8 a);//for circle
void drawCircle(SDL_Renderer *renderer, int n_cx, int n_cy, int radius, Uint8 r, Uint8 g, Uint8 b, Uint8 a);
void fillCircle(SDL_Renderer *renderer, int cx, int cy, int radius, Uint8 r, Uint8 g, Uint8 b, Uint8 a);

static std::vector<SDL_Rect> rectBatch;
static std::vector<SDL_Rect> rectBatchFill;
void drawRectangle(SDL_Renderer *renderer, int x, int y, int width, int height, Uint8 r, Uint8 g, Uint8 b, Uint8 a);
void fillRectangle(SDL_Renderer *renderer, int x, int y, int width, int height, Uint8 r, Uint8 g, Uint8 b, Uint8 a);

void drawLine(SDL_Renderer *renderer, int x1, int y1, int x2, int y2, Uint8 r, Uint8 g, Uint8 b, Uint8 a);

//SDL_Color
//std::map<SDL_Color, std::vector<SDL_Point>> pointsMap;
static std::vector<SDL_Point> pointsMap;
void drawPoint(SDL_Renderer *renderer, int x, int y, Uint8 r, Uint8 g, Uint8 b, Uint8 a);
void drawPoint(SDL_Renderer *renderer, Vector2D position, Color color);

void drawPrimitives(SDL_Renderer * renderer, Camera* camera);