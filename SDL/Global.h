#pragma once
#include <SDL.h>
#include <iostream>

const std::string BASE_FOLDER = "C:\\Users\\Paddy\\Desktop\\SDL\\";
const std::string CONTENT_PATH = BASE_FOLDER + "Content\\";
const std::string TEXTURE_PATH = CONTENT_PATH + "Textures\\";
const std::string AUDIO_PATH = CONTENT_PATH + "Audio\\";
const std::string FONT_PATH = CONTENT_PATH + "Fonts\\";

//defined in main file
extern SDL_Renderer * gRenderer;
extern float gDeltaTime;
