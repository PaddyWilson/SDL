#pragma once
#include <map>
#include <string>

#include "Component.h"
#include "Animation.h"

struct CAnimation :public Component<CAnimation>
{
	std::map<string, Animation> animations;
	string currentAnimation = "";
	string lastAnimation;
};