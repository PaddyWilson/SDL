#include "ISystem.h"

void ISystem::setWorld(World * world)
{
	this->world = world;
}

void ISystem::setEventSystem(EventSystem * eventSystem)
{
	this->eventSystem = eventSystem;
}

bool ISystem::hasComponents(Entity e)
{
	int count = 0;
	//test for required components
	for (size_t i = 0; i < requiredComponents.size(); i++) {
		auto bits = world->getEntityBitmask(e);//entity bitmask
		if (bits.test(i) && requiredComponents.test(i)) {
			count++;
		}
	}
	int k = requiredComponents.count();
	k = k;
	//does not have req comps
	if (count != requiredComponents.count())
	{
		return false;
	}

	//has comps and system has no optional ones
	if (optionalComponents.count() == 0)
	{
		//entities.push_back(e);
		return true;//no need to go futher in function
	}

	int op = 0;
	//test for optional components
	for (size_t i = 0; i < optionalComponents.size(); i++) {
		auto bits = world->getEntityBitmask(e);//entity bitmask
		if (bits.test(i) && optionalComponents.test(i)) {
			op++;
		}
	}

	if (count == requiredComponents.count() && op > 0) {
		return true;
		//entities.push_back(e);
	}
	return false;
}

void ISystem::addEntity(Entity e)
{
	if (hasComponents(e))
	{
		entities.push_back(e);
	}
}

void ISystem::removeEntity(Entity e)
{
	for (auto it = entities.begin(); it != entities.end(); ++it) {
		Entity entity = *it;
		if (entity.id == e.id) {
			entities.erase(it);
			return;
		}
	}
}
