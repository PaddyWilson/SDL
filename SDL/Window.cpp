
#include "Window.h"

Window::Window(std::string title, int x, int y, int width, int height, Uint32 flags) :
	title(title), x(x), y(y), width(width), height(height), flags(flags)
{
	window = NULL;
	mouseFocus = false;
	keyboardFocus = false;
	fullScreen = false;
	minimized = false;
	shown = false;
}

Window::~Window() {
	close();
}

bool Window::init()
{
	window = SDL_CreateWindow(title.c_str(), x, y, width, height, flags);
	if (window != NULL)
	{
		mouseFocus = true;
		keyboardFocus = true;

		//Grab window identifier
		windowID = SDL_GetWindowID(window);
		//Flag as opened
		shown = true;
	}
	return (window != NULL);
}

void Window::close()
{
	//Destroy window	
	SDL_DestroyWindow(window);
	window = NULL;
}

void Window::handleEvents(SDL_Event & e)
{
	if (e.type == SDL_WINDOWEVENT && e.window.windowID == windowID)
	{
		bool updateCaption = false;
		switch (e.window.event)
		{
			//Window appeared
		case SDL_WINDOWEVENT_SHOWN:
			shown = true;
			break;

			//Window disappeared
		case SDL_WINDOWEVENT_HIDDEN:
			shown = false;
			break;

			//Get new dimensions and repaint on window size change
		case SDL_WINDOWEVENT_SIZE_CHANGED:
			width = e.window.data1;
			height = e.window.data2;
			break;

			//Repaint on exposure
		case SDL_WINDOWEVENT_EXPOSED:
			//SDL_RenderPresent(renderer);
			break;

			//Mouse entered window
		case SDL_WINDOWEVENT_ENTER:
			mouseFocus = true;
			updateCaption = true;
			break;

			//Mouse left window
		case SDL_WINDOWEVENT_LEAVE:
			mouseFocus = false;
			updateCaption = true;
			break;

			//Window has keyboard focus
		case SDL_WINDOWEVENT_FOCUS_GAINED:
			keyboardFocus = true;
			updateCaption = true;
			break;

			//Window lost keyboard focus
		case SDL_WINDOWEVENT_FOCUS_LOST:
			keyboardFocus = false;
			updateCaption = true;
			break;
			//Window minimized
		case SDL_WINDOWEVENT_MINIMIZED:
			minimized = true;
			break;

			//Window maxized
		case SDL_WINDOWEVENT_MAXIMIZED:
			minimized = false;
			break;

			//Window restored
		case SDL_WINDOWEVENT_RESTORED:
			minimized = false;
			break;

			//Hide on close
		case SDL_WINDOWEVENT_CLOSE:
			SDL_HideWindow(window);
			break;
		}

		if (updateCaption)
		{
			std::stringstream ss;
			ss << "SDL Tutorial - MouseFocus:" << ((mouseFocus) ? "On" : "Off") << " KeyboardFocus:" << ((keyboardFocus) ? "On" : "Off");
			SDL_SetWindowTitle(window, ss.str().c_str());
			title = ss.str().c_str();
		}
	}

	//Enter exit full screen on return key
	else if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_RETURN)
	{
		if (fullScreen)
		{
			SDL_SetWindowFullscreen(window, SDL_FALSE);
			fullScreen = false;
		}
		else
		{
			SDL_SetWindowFullscreen(window, SDL_TRUE);
			fullScreen = true;
			minimized = false;
		}
	}
}

void Window::focus()
{
	if (!shown)
	{
		SDL_ShowWindow(window);
	}
	SDL_RaiseWindow(window);
}

