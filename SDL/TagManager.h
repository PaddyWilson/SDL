#pragma once
#include <map>
#include <vector>
#include <string>

#include "Entity.h"
//#include "EntityHandle.h"
//#include "World.h"

using namespace std;

//class EntityHandler;
//class World;

class TagManager
{
private:
	std::map<std::string, Entity> entities;
	//World *world;

public:
	void add(Entity e, std::string tag);

	Entity get(std::string tag);

	void remove(Entity e);
	void remove(std::string tag);

	std::vector<std::string> getTagList();

	void removeAll();
	int size() { return entities.size(); };
};