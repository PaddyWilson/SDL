#pragma once

#include <SDL.h>
#include <iostream>
#include <sstream>

class Window
{
public:
	Window(std::string title, int x, int y, int width, int height, Uint32 flags);
	~Window();

	bool init();
	void close();

	void handleEvents(SDL_Event& e);

	void focus();

	SDL_Window* getWindow() { return window; };

	std::string getTitle() { return title; };
	void setTitle(std::string title) { this->title = title; };

	int getWigth() { return width; };
	int getHeight() { return height; };

	int getWindowID() { return windowID; };

	bool hasMouseFocus() { return mouseFocus; };
	bool hasKeyboardFocus() { return keyboardFocus; };
	bool isMinimized() { return minimized; };
	bool isShown() { return shown; };

private:
	SDL_Window * window;
	int windowID;

	std::string title;

	int x, y;
	int width, height;
	Uint32 flags;

	bool mouseFocus;
	bool keyboardFocus;
	bool fullScreen;
	bool minimized;
	bool shown;
};

