#pragma once
#pragma once
#include <memory>

#include "ICommand.h"
#include "Camera.h"

#include "Global.h"

struct IMoveCam
{
	Camera* camera;

	IMoveCam(Camera* camera) {
		this->camera = camera;
	}
};

struct MoveCamUp :public IMoveCam, public ICommand
{
	MoveCamUp(Camera* camera) :
		IMoveCam(camera) {}

	void execute() override
	{
		camera->move(Vector2D(0, gDeltaTime * 250));
	};
};

struct MoveCamDown :public IMoveCam, public ICommand
{
	MoveCamDown(Camera* camera) :
		IMoveCam(camera) {}

	void execute() override
	{
		camera->move(Vector2D(0, gDeltaTime * -250));
	};
};

struct MoveCamLeft :public IMoveCam, public ICommand
{
	MoveCamLeft(Camera* camera) :
		IMoveCam(camera) {}

	void execute() override
	{
		camera->move(Vector2D(gDeltaTime * 250, 0));
	};
};

struct MoveCamRight :public IMoveCam, public ICommand
{
	MoveCamRight(Camera* camera) :
		IMoveCam(camera) {}

	void execute() override
	{
		camera->move(Vector2D(gDeltaTime * -250, 0));
	};
};