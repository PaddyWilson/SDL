#pragma once
#include <vector>
#include <queue>
#include <functional>

#include "EntityManager.h"
#include "EntityHandle.h"
#include "ComponentManager.h"
#include "ISystem.h"

#include "TagManager.h"
#include "GroupManager.h"

#include "Event.h"

using namespace std;

struct EntityHandle;
class ISystem;

class World
{
public:
	World();
	World(EventSystem * eventSystem);
	~World();

	//call after adding everything
	void init();

	EntityHandle createEntity();
	EntityHandle getEntity(int id);
	void removeEntity(Entity e);

	void addSystem(ISystem * system);

	//component things
	template<typename CompType>	CompType* addComponent(Entity e, CompType c);
	template<typename CompType> CompType* getComponent(Entity e);
	template<typename CompType>	void removeComponent(Entity e);

	//update things
	void preUpdate(double deltaTime);
	void update(double deltaTime);
	void postUpdate(double deltaTime);

	//tag things
	void addTag(Entity e, string tag);
	EntityHandle getTag(string tag);
	void removeTag(Entity e);
	void removeTag(string tag);

	//group things
	void addGroup(Entity e, string group);
	vector<Entity>* getGroup(Entity e);
	vector<Entity>* getGroup(string group);
	void removeFromGroup(Entity e);
	void removeFromGroup(Entity e, string group);
	void removeGroup(string group);

	//prefab things
	void addPrefab(string name, function<EntityHandle(float, float)> function);
	EntityHandle createPrefab(string name, float x, float y);

	//bitmask
	bitset<AMOUNT_OF_COMPONENTS> getEntityBitmask(Entity e);

	//count of things
	int getEntityCount();
	template<typename CompType>	int componentCount();
	int componentCount(int compId);
	vector<string> getCompNames() { return componentManagerNames; };

	//move these(make private)
	EntityManager entityManager;
	TagManager tagMgr;
	GroupManager groupMgr;

protected:

private:
	vector<ISystem*> systems;
	vector<BaseComponentManager*> componentManagers;
	vector<string> componentManagerNames;
	//bitmask for entities
	map<int, bitset<AMOUNT_OF_COMPONENTS>> entityBits;

	//flag to know if the world created it or was passed in
	bool createdEventSystem;
	EventSystem * eventSystem;

	//EntityManager entityManager;
	//TagManager tagMgr;
	//GroupManager groupMgr;

	//prefabs things
	map<string, function<EntityHandle(float, float)>> prefabMap;

	queue<Entity> addQueue;
	queue<Entity> removeQueue;

	void processAdd();
	void processRemove();
};

template<typename CompType>
CompType * World::addComponent(Entity e, CompType c)
{
	//create new comp manager if none exist
	if (componentManagers[getComponentID<CompType>()] == NULL) {
		componentManagers[getComponentID<CompType>()] = new ComponentManager<CompType>();
		componentManagerNames[getComponentID<CompType>()] = getComponentName<CompType>();
	}

	typedef ComponentManager<CompType> CompMgrType;
	CompMgrType* mgr = static_cast<CompMgrType*>(componentManagers[getComponentID<CompType>()]);

	entityBits[e.id].set(getComponentID<CompType>(), true);

	return mgr->add(e, c);
}

template<typename CompType>
inline CompType * World::getComponent(Entity e)
{
	//create new comp manager if none exist
	if (componentManagers[getComponentID<CompType>()] == NULL)
		return nullptr;//componentManagers[getComponentID<CompType>()] = new ComponentManager<CompType>();

	//entity does not have this component
	if (entityBits[e.id][getComponentID<CompType>()] == false)
		return nullptr;

	typedef ComponentManager<CompType> CompMgrType;
	CompMgrType* mgr = static_cast<CompMgrType*>(componentManagers[getComponentID<CompType>()]);

	return mgr->get(e);
}

template<typename CompType>
inline void World::removeComponent(Entity e)
{
	//create new comp manager if none exist
	if (componentManagers[getComponentID<CompType>()] == NULL)
		return nullptr;//componentManagers[getComponentID<CompType>()] = new ComponentManager<CompType>();

	//entity does not have this component
	if (entityBits[e.id][getComponentID<CompType>()] == false)
		return;

	typedef ComponentManager<CompType> CompMgrType;
	CompMgrType* mgr = static_cast<CompMgrType*>(componentManagers[getComponentID<CompType>()]);
	entityBits[e.id].set(getComponentID<CompType>(), false);
	mgr->remove(e);
}

template<typename CompType>
inline int World::componentCount()
{
	if (componentManagers[getComponentID<CompType>()] == NULL)
		return 0;

	typedef ComponentManager<CompType> CompMgrType;
	CompMgrType* mgr = static_cast<CompMgrType*>(componentManagers[getComponentID<CompType>()]);
	return mgr->size();
}
