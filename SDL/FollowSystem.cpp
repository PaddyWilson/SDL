#include "FollowSystem.h"

FollowSystem::FollowSystem()
{
	requiredComponents.set(getComponentID<Transform>(), true);
	requiredComponents.set(getComponentID<Movement>(), true);
	requiredComponents.set(getComponentID<Follow>(), true);
}

FollowSystem::~FollowSystem() {}

void FollowSystem::init()
{

}

void FollowSystem::update(double dt)
{
	for (auto e : entities)
	{
		EntityHandle handle;
		handle.entity = e;
		handle.world = world;

		Transform *trans = handle.getComponent<Transform>();
		Movement *move = handle.getComponent<Movement>();
		Follow *follow = handle.getComponent<Follow>();

		EntityHandle h2 = world->getEntity(follow->entityToFollow.id);
		if (!h2.hasComponent<Transform>()) {
			continue;
		}

		Transform *toFollow = h2.getComponent<Transform>();

		float dis = trans->position.distance(toFollow->position);
		//in the distance of the entity
		if (dis < follow->radiusToFollow)
		{
			move->accecleration = Vector2D();
			move->velocity = Vector2D();
		}
		//not in radius
		else
		{
			Vector2D moveDirNorm = Vector2D::normalize(trans->position, toFollow->position);
			move->velocity = (moveDirNorm * -1) * follow->followSpeed;
		}
	}
}
