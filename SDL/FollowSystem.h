#pragma once
#include "ISystem.h"
#include "CompIncludes.h"

class FollowSystem : public ISystem
{
private:

public:
	FollowSystem();
	~FollowSystem();

	void init() override;
	void update(double dt) override;
};