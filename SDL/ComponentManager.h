#pragma once
#include <map>
#include <memory>

#include "Component.h"
#include "Entity.h"

using namespace std;

class BaseComponentManager {
public:
	virtual void remove(Entity e) {};
	virtual int size() { return 0; };
};

template<typename ComponentType>
class ComponentManager : public BaseComponentManager
{
private:
	map<int, ComponentType> components;
	string compName;
public:
	ComponentManager();
	ComponentType* add(Entity e, ComponentType c);
	ComponentType* get(Entity e);
	void remove(Entity e) override;
	int size() override;
	string name();
};

template<typename ComponentType>
inline ComponentManager<ComponentType>::ComponentManager()
{
}

template<typename ComponentType>
ComponentType * ComponentManager<ComponentType>::add(Entity e, ComponentType c)
{
	ComponentType newComp;
	components[e.id] = c;
	return &components[e.id];
}

template<typename ComponentType>
ComponentType * ComponentManager<ComponentType>::get(Entity e)
{
	return &components[e.id];
}

template<typename ComponentType>
void ComponentManager<ComponentType>::remove(Entity e)
{
	components.erase(e.id);
}

template<typename ComponentType>
inline int ComponentManager<ComponentType>::size()
{
	return components.size();
}

template<typename ComponentType>
inline string ComponentManager<ComponentType>::name()
{
	return ;
}
