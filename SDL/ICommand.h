#pragma once

#include <functional>

struct ICommand
{
	virtual ~ICommand() {};
	virtual void execute() {};
};

struct LamdaCommand : ICommand
{
	LamdaCommand() { lamda = nullptr; };
	LamdaCommand(std::function<void()> command)
	{
		lamda = command;
	};

	virtual void execute() override
	{
		if (lamda != nullptr)
		{
			lamda();
		}
	};

	std::function<void()> lamda;
};