#include "Line.h"

Line::Line() 
{
	point1 = Vector2D(0, 0);
	point2 = Vector2D(0, 0);
}
Line::Line(Vector2D p1, Vector2D p2)
{
	point1 = p1;
	point2 = p2;
}
Line::Line(float x1, float y1, float x2, float y2)
{
	point1.x = x1;	point1.y = y1;
	point2.x = x2;	point2.y = y2;
}

bool Line::collision(Line line)
{
	Vector2D b = this->point2 - this->point1;
	Vector2D d = line.point2 - line.point1;
	float DotDPerp = b.x * d.y - b.y * d.x;

	// if DotdPerp == 0, it means the lines are parallel
	if (DotDPerp == 0)
		return false;

	Vector2D c = line.point1 - this->point1;
	float t = (c.x * d.y - c.y * d.x) / DotDPerp;
	if (t < 0 || t > 1)
		return false;

	float u = (c.x * b.y - c.y * b.x) / DotDPerp;
	if (u < 0 || u > 1)
		return false;

	return true;
}

bool Line::collisionLine(float x1, float y1, float x2, float y2)
{
	return collision(Line(x1, y1, x2, y2));
}

bool Line::collision(Circle circle)
{
	return circle.collision(*this);
}

bool Line::collision(Rectanglef rec)
{
	return rec.collision(*this);
}
