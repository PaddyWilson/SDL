#include "AudioManager.h"

//global audio manager
AudioManager gAudioMgr;

AudioManager::AudioManager(){}

AudioManager::~AudioManager()
{
	for (auto audio : music)
	{
		if (audio.second != NULL)
		{
			delete audio.second;
			audio.second = NULL;
		}
	}
}

bool AudioManager::load(string name, string path, bool isMusic, bool fromContent)
{
	Music* m = NULL;
	string p = "";
	bool loaded = false;

	if (fromContent)
		p += AUDIO_PATH + path;
	else
		p = path;

	if (isMusic)
	{
		m = new Music();
		loaded = m->load(p);
	}
	else
	{
		m = new SoundEffect();
		loaded = m->load(p);
	}

	if (!loaded)
		return false;
	
	if (music[name] != NULL)
	{
		return false;
	}

	music[name] = m;
	return true;
}

void AudioManager::play(string name)
{
	if (music[name] != NULL)
	{
		music[name]->play();
	}
}

void AudioManager::pause()
{
	Mix_PauseMusic();
}

void AudioManager::resume()
{
	Mix_ResumeMusic();
}

void AudioManager::stop()
{
	Mix_HaltMusic();
}

bool AudioManager::add(string name, string path, bool isMusic, bool fromContent)
{
	return load(name, path, isMusic, fromContent);
}

Music * AudioManager::get(string name)
{
	return music[name];
}

void AudioManager::remove(string name)
{
	if (music[name] != NULL)
	{
		delete music[name];
		music[name] = NULL;
	}
}

int AudioManager::getCount()
{
	return music.size();
}
