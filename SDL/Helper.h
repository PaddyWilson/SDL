#pragma once
#include <vector>
#include <string>
#include <iostream>
#include <filesystem>
#include <sstream>
#include <iterator>

namespace fs = std::experimental::filesystem;

std::vector<std::string> getFilesInFolder(std::string folder);

std::vector<std::string> splitString(std::string text, char c);