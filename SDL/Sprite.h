#pragma once

#include <SDL.h>
#include <string>

#include "Texture.h"
#include "Vector2D.h"

class Sprite
{

private:
	std::string textureName;
	Texture* texture;
	SDL_Rect subRect;
	int width, height;

public:
	Sprite();
	Sprite(std::string texture, int x = 0, int y = 0, int width = 0, int height = 0);
	Sprite(std::string texture, SDL_Rect subRect = { 0, 0, 0, 0 });
	~Sprite();

	void render(SDL_Renderer* renderer, float x, float y);
	void render(SDL_Renderer* renderer, Vector2D vec);

	Vector2Di size() { return Vector2Di(width, height); };
private:
	void init(std::string textureName, SDL_Rect subRect = { 0, 0, 0, 0 });
};