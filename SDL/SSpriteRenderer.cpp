#include "SSpriteRenderer.h"

SSpriteRenderer::SSpriteRenderer(SDL_Renderer * renderer, Camera * camera)
{
	this->renderer = renderer;
	this->camera = camera;

	requiredComponents.set(getComponentID<Transform>(), true);
	requiredComponents.set(getComponentID<SpriteRenderer>(), true);
}

SSpriteRenderer::~SSpriteRenderer() {}

void SSpriteRenderer::init()
{
	//sub to events
	eventSystem->subscribe(this, &SSpriteRenderer::onCollisionEvent);
}

void SSpriteRenderer::update(double deltaTime)
{
	//Grid<Info> grid;
	//cout << "SP update " << entities.size() << endl;
	for (size_t i = 0; i < entities.size(); i++)
	{
		Transform* trans = world->getComponent<Transform>(entities[i]);
		SpriteRenderer* sr = world->getComponent<SpriteRenderer>(entities[i]);

		Vector2D position = trans->position + sr->offset;
		if (camera != NULL)
		{
			position += camera->getPostion();
		}

		if (sr->sprite != nullptr)
		{
			//grid.insert(Rectanglef(position, sr->sprite->size().convert<float>()), Info(trans, sr));
			SDL_SetRenderDrawColor(renderer, sr->color.r, sr->color.g, sr->color.b, sr->color.a);
			sr->sprite->render(renderer, position.x, position.y);
		}
	}

	/*vector<Info> items;
	if (camera == nullptr) {
		grid.retrieve(Rectanglef(0, 0, 1280, 720), &items);
	}
	else
	{
		grid.retrieve(Rectanglef(camera->position, camera->screenSize), &items);
	}

	for (size_t i = 0; i < items.size(); i++)
	{
		Transform* trans = items[i].t;
		SpriteRenderer* sr = items[i].s;
		Vector2D position = trans->position + sr->offset;

		if (camera != NULL)
			position += camera->getPostion();

		SDL_SetRenderDrawColor(renderer, sr->color.r, sr->color.g, sr->color.b, sr->color.a);
		sr->sprite->render(renderer, position.x, position.y);
	}
*/
}

void SSpriteRenderer::onCollisionEvent(CollisionEvent * collision)
{
	//collision->e1.getComponent<SpriteRenderer>()->sprite = gSpriteManager.getSprite("crate_07");


	//cout << "onCollisionEvent " << collision->e1.entity.id << " hit " << collision->e2.entity.id << endl;
}
