#pragma once
#include <memory>

#include "ICommand.h"
#include "Entity.h"

#include "CompIncludes.h"

struct IMove
{
	Entity entity;
	Transform* transform;
	Movement* movement;

	IMove(Entity entity) {
		this->entity = entity;
		//transform = entity->get<Transform>();
		//movement = entity->get<Movement>();
	}
};

struct MoveUp :public IMove, public ICommand
{
	MoveUp(Entity entity) :
		IMove(entity) {}

	void execute() override
	{
		movement->velocity = Vector2D(0, -100);
	};
};

struct MoveDown :public IMove, public ICommand
{
	MoveDown(Entity entity) :
		IMove(entity) {}

	void execute() override
	{
		movement->velocity = Vector2D(0, 100);
	};
};

struct MoveLeft :public IMove, public ICommand
{
	MoveLeft(Entity entity) :
		IMove(entity) {}

	void execute() override
	{
		movement->velocity = Vector2D(-100, 0);
	};
};

struct MoveRight :public IMove, public ICommand
{
	MoveRight(Entity entity) :
		IMove(entity) {}

	void execute() override
	{
		movement->velocity = Vector2D(100, 0);
	};
};