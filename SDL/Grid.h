#pragma once
#include <vector>
#include <map>
#include <math.h>

#include "Rectangle.h"
#include "PrimitiveDrawing.h"

using namespace std;

struct GridPosition {
	int x, y;
};

inline bool operator==(const GridPosition& lhs, const GridPosition& rhs) {
	return lhs.x == rhs.x && lhs.y == rhs.y;
}
inline bool operator!=(const GridPosition& lhs, const GridPosition& rhs) { return !operator==(lhs, rhs); }
inline bool operator< (const GridPosition& lhs, const GridPosition& rhs) {
	return lhs.x < rhs.x && lhs.y < rhs.y;
}
inline bool operator> (const GridPosition& lhs, const GridPosition& rhs) { return  operator< (rhs, lhs); }
inline bool operator<=(const GridPosition& lhs, const GridPosition& rhs) { return !operator> (lhs, rhs); }
inline bool operator>=(const GridPosition& lhs, const GridPosition& rhs) { return !operator< (lhs, rhs); }

template<typename Type>
class Grid
{
public:
	Grid();
	Grid(int width, int height) : recWidth(width), recHeight(height) {};
	~Grid();

	void insert(Rectanglef rectangle, Type object);
	void remove(Type type);
	void retrieve(Rectanglef rectangle, vector<Type> *objectOutput);
	void clear();

	void debugDraw();

private:
	int recWidth = 32;
	int recHeight = 32;

	//map<string, vector<Type>*> grid;//stores the index into the vector
	map<string, vector<Type>> grid;//stores the index into the vector
	map<string, GridPosition> positions;
	//vector<GridPosition> positions;
};

template<typename Type>
inline Grid<Type>::Grid() {}

template<typename Type>
inline Grid<Type>::~Grid()
{
	clear();
}

template<typename Type>
inline void Grid<Type>::insert(Rectanglef rectangle, Type object)
{
	//get the grid index
	GridPosition position;
	position.x = (rectangle.x / recWidth);
	position.y = (rectangle.y / recHeight);

	//get the heightest possable grid position
	GridPosition highPosition;
	highPosition.x = (rectangle.x + rectangle.width) / recWidth;
	highPosition.y = (rectangle.y + rectangle.height) / recHeight;

	//loop through all positions in between position and high position
	for (int y = position.y; y <= highPosition.y; y++) {
		for (int x = position.x; x <= highPosition.x; x++)
		{
			GridPosition pos;
			pos.x = x;
			pos.y = y;
			string spos = to_string(x) + ":" + to_string(y);
			//insert
			grid[spos].push_back(object);
			//grid[pos].push_back(object);

			positions[spos] = pos;
		}
	}
}

template<typename Type>
inline void Grid<Type>::remove(Type type)
{
	for (auto &item : grid)
	{
		//if (item.second == nullptr) continue;

		auto it = std::find(item.second.begin(), item.second.end(), type);
		if (it != item.second.end()) {
			item.second.erase(it);
		}
	}
}


template<typename Type>
inline void Grid<Type>::retrieve(Rectanglef rectangle, vector<Type>* objectOutput)
{
	//get the grid index
	GridPosition position;
	position.x = (rectangle.x / recWidth);
	position.y = (rectangle.y / recHeight);
	//get the heightest possable grid position
	GridPosition highPosition;
	highPosition.x = (rectangle.x + rectangle.width) / recWidth;
	highPosition.y = (rectangle.y + rectangle.height) / recHeight;
	
	//loop through all positions in between position and high position
	for (int y = position.y; y <= highPosition.y; y++) {
		for (int x = position.x; x <= highPosition.x; x++)
		{
			//GridPosition pos;
			//pos.x = x;
			//pos.y = y;
			string pos = to_string(x) + ":" + to_string(y);

			/*for (int i = 0; i < grid[pos].size(); i++)
			{
				objectOutput->push_back(grid[pos][i]);
			}*/

			//find dupes
			//for (auto &it : *grid[pos]) {
			//	if (std::find(objectOutput->begin(), objectOutput->end(), it) == objectOutput->end()) {
			//		// someName not in name, add it
			//		objectOutput->push_back(it);
			//	}
			//}
			objectOutput->insert(objectOutput->end(), grid[pos].begin(), grid[pos].end());
		}
	}
}

template<typename Type>
inline void Grid<Type>::clear()
{
	grid.clear();
	positions.clear();
}

template<typename Type>
inline void Grid<Type>::debugDraw()
{
	for (auto &item : positions)
	{
		drawRectangle(NULL, (item.second.x * recWidth), (item.second.y * recHeight), recWidth, recHeight, 0, 0, 0, 0);
	}
}
