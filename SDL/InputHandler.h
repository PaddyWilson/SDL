#pragma once
#include <SDL.h>
#include <map>
#include <queue>

#include "ICommand.h"

class InputHandler
{
public:
	enum KeyState {
		None = 0,
		Down = 1,
		Up = 2,
		Pressed = 4,
		Released = 8
	};

public:
	InputHandler();
	~InputHandler();

	void update();
	//void updateInput(SDL_Event& e);
	void processCommands();

	void mapKey(SDL_Scancode key, ICommand* command);
	void mapKey(SDL_Scancode key, ICommand* command, KeyState state);

	void mapKey(SDL_Scancode key, std::function<void()> command);
	void mapKey(SDL_Scancode key, std::function<void()> command, KeyState state);

	void unmapKey(SDL_Scancode key);
private:
	Uint8 * previousKeys;//[SDL_NUM_SCANCODES];
	Uint8 * currentKeys;//[SDL_NUM_SCANCODES];

	std::map<SDL_Scancode, ICommand*> keyCommand;
	std::map<SDL_Scancode, KeyState> keyCommandState;

	std::queue<ICommand*> commandQueue;
};

