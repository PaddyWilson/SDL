#pragma once
#include "Component.h"
#include "Sprite.h"
#include "SpriteManager.h"
#include "Color.h"
#include "Vector2D.h"

struct SpriteRenderer : public Component<SpriteRenderer>
{
	SpriteRenderer() {
		this->sprite = nullptr;
		this->offset = Vector2D();
		flipX = false;
		flipY = false;
		layer = 0;
	};

	SpriteRenderer(Sprite *sprite, Vector2D offset = Vector2D(), Color color = Color()) {
		this->sprite = sprite;
		this->offset = offset;
		flipX = false;
		flipY = false;
		layer = 0;
	};

	SpriteRenderer(std::string spriteName, Vector2D offset = Vector2D(), Color color = Color()) {
		this->sprite = gSpriteManager.getSprite(spriteName);
		this->color = color;
		this->offset = offset;
		flipX = false;
		flipY = false;
		layer = 0;
	};
	//~SpriteRenderer();

	Sprite * sprite;
	Color color;
	Vector2D offset;
	bool flipX, flipY;
	int layer;
};

