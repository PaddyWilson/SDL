#pragma once
#include <cmath>
#include "Vector2D.h"
#include "Line.h"
#include "Rectangle.h"

//forward declare becasue circular dependency
template <typename T> struct RectangleBase;
using Rectanglef = RectangleBase<float>;

struct Line;

struct Circle
{
	Circle() { 
		position = Vector2D();
		radius = 0.5f;
	};
	
	Circle(Vector2D pos, float radius) {
		position = pos; 
		this->radius = radius;
	};

	inline void move(Vector2D vec) {
		position += vec;
	};

	bool collision(Circle c);
	bool collision(Vector2D v);
	bool collision(Rectanglef r);
	bool collision(class Line l);

	Vector2D position;
	float radius;
};