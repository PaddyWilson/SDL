#pragma once
#include <SDL.h>
#include "Component.h"
#include "Vector2D.h"
#include "Rectangle.h"

#include <iostream>
#include "Global.h"
#include "PrimitiveDrawing.h"

enum ColliderType {
	NoneType, BoxType, CircleType, LineType, PointType
};

struct BaseCollider
{
	ColliderType type;
	Vector2D offset;
	bool isTrigger;//not used

	//rec 
	Vector2D size;
	//cir radius
	float radius;
	//line transform to point(Vector2D size)
	Vector2D point1;
	Vector2D point2;
	//point is the transform of entity

	BaseCollider() { type = ColliderType::NoneType; };//remove when the other collider components are gone
	BaseCollider(ColliderType type, Vector2D vec1 = Vector2D(), Vector2D vec2 = Vector2D(), Vector2D vec3 = Vector2D())
	{//point
		this->type = type;
		switch (type)
		{
		case BoxType:
			size = vec1;
			offset = vec2;
			break;

		case LineType:
			point1 = vec1;
			point2 = vec2;
			offset = vec3;
			break;

		case PointType:
			offset = vec1;
			break;

		case CircleType:
		case NoneType:
		default:
			break;
		}
	};
	//BaseCollider(ColliderType type, Vector2D size, Vector2D offest = Vector2D())
	//{//box
	//	this->type = type;
	//	this->offset = offest;
	//	this->size = size;
	//};
	BaseCollider(ColliderType type, float radius, Vector2D offest = Vector2D())
	{//circle
		this->type = type;
		this->offset = offest;
		this->radius = radius;
	};

	virtual Rectanglef getAABB(Vector2D position)
	{
		if (type == ColliderType::BoxType)
		{
			return Rectanglef(position + offset, size);
		}
		else if (type == ColliderType::CircleType)
		{
			Vector2D pos = (position + offset) - radius;
			float size = radius * 2;
			return Rectanglef(pos.x, pos.y, size, size);
		}
		else if (type == ColliderType::LineType)
		{
			Vector2D p1 = position + point1 + offset;
			Vector2D p2 = position + point2 + offset;

			//rec bounds
			Vector2D topLeft;
			if (p1.x < p2.x)
				topLeft.x = p1.x;
			else
				topLeft.x = p2.x;

			if (p1.y < p2.y)
				topLeft.y = p1.y;
			else
				topLeft.y = p2.y;
			//topLeft.y = p1.y;

			//topLeft.x = (position.x - point2.x) + offset.x;
			//topLeft.y = (position.y - point1.y) + offset.y;

			Vector2D bottomRight;
			if (p1.x > p2.x)
				bottomRight.x = point1.x;
			else
				bottomRight.x = point2.x;

			if (p1.y > p2.y)
				bottomRight.y = point1.y;
			else
				bottomRight.y = point2.y;

			//bottomRight.x = (point2.x);
			//bottomRight.y = (point1.y);

			//std::cout << "1 " << topLeft.x << ":" << topLeft.y << " " << bottomRight.x << ":" << bottomRight.y << std::endl;
			//Vector2D p2 = size + position + offset;			
			return Rectanglef(topLeft, bottomRight);
		}
		else if (type == ColliderType::PointType)
		{
			return Rectanglef(position + offset, Vector2D());
		}
		return Rectanglef();
	};

	virtual bool interects(Vector2D postions1, BaseCollider* base, Vector2D postions2) {
		Vector2D p1, p2;
		Rectanglef r1, r2;
		Circle c1, c2;
		Line l1, l2;

		//make the shapes
		switch (type)
		{
		case BoxType:
			r1 = Rectanglef(postions1 + this->offset, this->size);
			break;
		case CircleType:
			c1 = Circle(postions1 + this->offset, this->radius);
			break;
		case PointType:
			p1 = Vector2D(postions1 + this->offset);
			break;
		case LineType:
			l1 = Line(postions1 + point1 + this->offset, postions1 + point2 + this->offset);
			break;
		default:
			break;
		}

		switch (base->type)
		{
		case BoxType:
			r2 = Rectanglef(postions2 + base->offset, base->size);
			break;
		case CircleType:
			c2 = Circle(postions2 + base->offset, base->radius);
			break;
		case PointType:
			p2 = Vector2D(postions2 + base->offset);
			break;
		case LineType:
			l2 = Line(postions2 + base->point1 + base->offset, postions2 + base->point2 + base->offset);
			break;
		default:
			break;
		}

		//check to intersects
		switch (this->type)
		{
		case BoxType:
			switch (base->type)
			{
			case BoxType:
				return (r1.collision(r2));
				break;
			case CircleType:
				return (r1.collision(c2));
				break;
			case PointType:
				return (r1.collision(p2));
				break;
			case LineType:
				return (r1.collision(l2));
				break;
			default:
				break;
			}
			break;
		case CircleType:
			switch (base->type)
			{
			case BoxType:
				return (c1.collision(r2));
				break;
			case CircleType:
				return (c1.collision(c2));
				break;
			case PointType:
				return (c1.collision(p2));
				break;
			case LineType:
				return (c1.collision(l2));
				break;
			default:
				break;
			}
			break;
		case PointType:
			//point(Vector2D) has no collison functions
			switch (base->type)
			{
			case BoxType:
				return (p1.collision(r2));
				break;
			case CircleType:
				return (p1.collision(c2));
				break;
				//case PointType:
					//return (p1.collision(p2));
					//break;
				//case LineType:
					//return (p1.collision(l2));
					//break;
			default:
				break;
			}
			break;
		case LineType:
			switch (base->type)
			{
			case BoxType:
				return (l1.collision(r2));
				break;
			case CircleType:
				return (l1.collision(c2));
				break;
			case PointType:
				// #### Line has no point collision function
				//return (l1.collision(p2));
				break;
			case LineType:
				return (l1.collision(l2));
				break;
			default:
				break;
			}
			break;
		default:
			break;
		}

		return false;
	};//end interects

	virtual void debugDraw(SDL_Renderer* renderer, Vector2D postion) {};
};
