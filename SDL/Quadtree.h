#pragma once
#include <vector>
#include <memory>

#include "Vector2D.h"
#include "Rectangle.h"
#include "Entity.h"

#include "PrimitiveDrawing.h"

template<typename T>
class Quadtree
{
public:
	//Quadtree();
	Quadtree(int pLevel, Rectanglef pBounds);
	~Quadtree();

	void clear();
	void split();
	int getIndex(Rectanglef pRect);
	void insert(Rectanglef pRect, T entity);
	std::vector<T> retrieve(Rectanglef pRect);
	std::vector<T> retrieve(Rectanglef pRect, vector<T> *returnObjects);

	void debugDraw();

private:
	int MAX_OBJECTS = 20;
	int MAX_LEVELS = 15;

	int level;
	std::vector<Rectanglef> objectsBounds;
	std::vector<T> objects;
	Rectanglef bounds;
	Quadtree *nodes[4];
};

template<typename T>
Quadtree<T>::Quadtree(int pLevel, Rectanglef pBounds)
{
	level = pLevel;
	//objects.capacity = MAX_OBJECTS;
	bounds = pBounds;
}

template<typename T>
Quadtree<T>::~Quadtree()
{
	objects.clear();
	objectsBounds.clear();

	for (size_t i = 0; i < 4; i++)
	{
		delete nodes[i];
	}
}

template<typename T>
void Quadtree<T>::clear()
{
	objects.clear();
	objectsBounds.clear();

	for (size_t i = 0; i < 4; i++)
	{
		if (nodes[i] != NULL)
		{
			nodes[i]->clear();

			//clean up pointers
			delete nodes[i];

			nodes[i] = NULL;
		}
	}
}

template<typename T>
void Quadtree<T>::split()
{
	int subWidth = bounds.width / 2;
	int subHeight = bounds.height / 2;

	int x = bounds.x;
	int y = bounds.y;

	nodes[0] = new Quadtree(level + 1, Rectanglef(x + subWidth, y, subWidth, subHeight));//top right
	nodes[1] = new Quadtree(level + 1, Rectanglef(x, y, subWidth, subHeight));//top left
	nodes[2] = new Quadtree(level + 1, Rectanglef(x, y + subHeight, subWidth, subHeight));//bottom left
	nodes[3] = new Quadtree(level + 1, Rectanglef(x + subWidth, y + subHeight, subWidth, subHeight));//bottom right
}

template<typename T>
int Quadtree<T>::getIndex(Rectanglef pRect)
{
	int index = -1;
	double verticalMidpoint = bounds.x + (bounds.width / 2);
	double horizontalMidpoint = bounds.y + (bounds.height / 2);

	// Object can completely fit within the top quadrants
	bool topQuadrant = (pRect.y < horizontalMidpoint && pRect.y + pRect.height < horizontalMidpoint);
	// Object can completely fit within the bottom quadrants
	bool bottomQuadrant = (pRect.y > horizontalMidpoint);

	// Object can completely fit within the left quadrants
	if (pRect.x < verticalMidpoint && pRect.x + pRect.width < verticalMidpoint) {
		if (topQuadrant) {
			index = 1;
		}
		else if (bottomQuadrant) {
			index = 2;
		}
	}
	// Object can completely fit within the right quadrants
	else if (pRect.x > verticalMidpoint) {
		if (topQuadrant) {
			index = 0;
		}
		else if (bottomQuadrant) {
			index = 3;
		}
	}

	return index;
}

template<typename T>
void Quadtree<T>::insert(Rectanglef pRect, T entity)
{
	//has sub nodes
	if (nodes[0] != NULL) {
		int index = getIndex(pRect);//get the node to insert into

		if (index != -1) {
			nodes[index]->insert(pRect, entity);

			return;
		}
	}

	//add object to this quadtree
	objectsBounds.push_back(pRect);
	objects.push_back(entity);

	//if their are more objects than than MAX_COUNT
	if (objects.size() > MAX_OBJECTS && level < MAX_LEVELS) {
		//make quadtree nodes to hold more objects if there are none
		if (nodes[0] == NULL) {
			split();
		}

		//loop through objects and place the ones into the other nodes
		int i = 0;
		while (i < objects.size()) {
			int index = getIndex(objectsBounds[i]);
			if (index != -1) {
				T ent = objects[i];
				auto item = std::find(objects.begin(), objects.end(), (T)ent);
				if (item == objects.end())
					return;
				objects.erase(item);

				Rectanglef rec = objectsBounds[i];
				auto item2 = std::find(objectsBounds.begin(), objectsBounds.end(), (Rectanglef)rec);
				if (item2 == objectsBounds.end())
					return;
				objectsBounds.erase(item2);

				nodes[index]->insert(rec, ent);
			}
			else {
				i++;
			}
		}
	}
}

template<typename T>
std::vector<T> Quadtree<T>::retrieve(Rectanglef pRect)
{
	vector<T> returnObjects;
	return retrieve(pRect, &returnObjects);
}

template<typename T>
std::vector<T> Quadtree<T>::retrieve(Rectanglef pRect, vector<T>* returnObjects)
{
	//the bounds do not intersect with the search rect
	//if (!pRect.collision(bounds)) return *returnObjects;//move on
	//
	////find the quadtrees the rect intersets with
	//for (int i = 0; i < 4; i++)
	//{
	//	if (nodes[i] != NULL)
	//		nodes[i]->retrieve(pRect, returnObjects);
	//}

	////at the last node. add the objects stored
	//if (nodes[0] == NULL)
	//{
	//	returnObjects->insert(returnObjects->end(), objects.begin(), objects.end());
	//}
	
	/* this way does not get all the objects in the bounds */
	int index = getIndex(pRect);
	if (index != -1 && nodes[0] != NULL) {
		nodes[index]->retrieve(pRect, returnObjects);
	}
	returnObjects->insert(returnObjects->end(), objects.begin(), objects.end());

	return *returnObjects;
}

template<typename T>
void Quadtree<T>::debugDraw()
{
	drawRectangle(NULL, bounds.x, bounds.y, bounds.width, bounds.height, 0, 0, 0, 0);

	for (int i = 0; i < 4; i++)
	{
		if (nodes[i] != NULL)
			nodes[i]->debugDraw();
	}
}
