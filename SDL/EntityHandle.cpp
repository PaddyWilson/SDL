#include "EntityHandle.h"

void EntityHandle::destroy()
{
	world->removeEntity(entity);
}

bitset<AMOUNT_OF_COMPONENTS> EntityHandle::getBitmask()
{
	return world->getEntityBitmask(entity);
}

void EntityHandle::addTag(string tag)
{
	world->addTag(entity, tag);
}

void EntityHandle::removeTag()
{
	world->removeTag(entity);
}

void EntityHandle::addGroup(string group)
{
	world->addGroup(entity, group);
}

vector<Entity>* EntityHandle::getGroup()
{
	return world->getGroup(entity);
}

void EntityHandle::removeGroup()
{
	world->removeFromGroup(entity);
}

void EntityHandle::removeGroup(string group)
{
	world->removeFromGroup(entity, group);
}
