#pragma once
#include "Entity.h"
#include "World.h"

class World;

struct EntityHandle
{
	Entity entity;
	World *world;

	void destroy();

	template<typename CompType>	
	CompType* addComponent(CompType c) {
		return world->addComponent(entity, c);
	};

	template<typename CompType>
	CompType* getComponent() {
		return world->getComponent<CompType>(entity);
	};

	template<typename CompType>	
	void removeComponent() {
		world->removeComponent<CompType>(entity);
	};

	template<typename CompType>
	bool hasComponent() {
		return world->getEntityBitmask(entity).test(getComponentID<CompType>());
	};

	bitset<AMOUNT_OF_COMPONENTS> getBitmask();

	//tag things
	void addTag(string tag);
	void removeTag();

	//group things
	void addGroup(string group);
	vector<Entity>* getGroup();
	void removeGroup();
	void removeGroup(string group);

};