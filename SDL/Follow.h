#pragma once
#include "Component.h"
#include "Entity.h"

struct Follow :public Component<Follow>
{
	Entity entityToFollow;
	float radiusToFollow;
	float followSpeed;
};