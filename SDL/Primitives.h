#pragma once
#include "Component.h"
#include "Rectangle.h"

enum MeshType {
	Rectangle,
	MultiRectangle,
	RectangleFill,
	MultiRectangleFill,

	LineType,
	MultiLine,
	
	PointType,
	Points
};

struct PrimitiveMesh : public Component<PrimitiveMesh>, public Rectanglef
{
	PrimitiveMesh() 
	{
		x = 0;
		y = 0;
		height = 0;
		width = 0;
	};

	PrimitiveMesh(Rectanglef rec) 
	{
		x = rec.x;
		y = rec.y;
		width = rec.width;
		height = rec.height;
	};
};