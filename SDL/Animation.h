#pragma once
#include <vector>
#include "Timer.h"
#include "Sprite.h"

struct AnimationFrame {
	Sprite *sprite;
	Vector2D offset;
	int time;
};

class Animation
{
private:
	vector<AnimationFrame> frames;
	int currentFrame = 0;
	int ticks = 0;

	Timer timer;
	bool loops;

	bool stoped;
	bool paused;

public:
	Animation(bool loop = false);
	void addFrame(Sprite *sprite, int time, Vector2D offset = Vector2D());

	AnimationFrame* getNextFrame();

	void start();
	void stop();
	void pause();
	void restart();
	bool isPaused() { return paused; };
	bool isStoped() { return stoped; };
};