
#include "GroupManager.h"

GroupManager::~GroupManager()
{
	removeAll();
}

void GroupManager::add(Entity e, string group)
{
	if (group == "")
		return;

	if (groups.find(group) == groups.end())
	{
		vector<Entity> *n = new vector<Entity>();
		groups[group] = n;
	}

	groups[group]->push_back(e);
}

vector<Entity>* GroupManager::get(Entity e)
{
	//loop through groups map
	for (auto group : groups)
	{
		//take list for group and seach for entity
		auto it = std::find(group.second->begin(), group.second->end(), e);
		if (it != group.second->end())
		{
			return group.second;
		}
	}
	return &emptyGroup;
}

vector<Entity>* GroupManager::get(string group)
{
	if (groups.find(group) == groups.end())
	{
		return &emptyGroup;
	}
	return groups[group];
}

vector<string> GroupManager::getGroupList()
{
	vector<string>items;

	for (auto i : groups)
		items.push_back(i.first);

	return items;
}

void GroupManager::remove(Entity e)
{
	//loop through groups map
	for (auto group : groups)
	{
		remove(e, group.first);
		//take list for group and seach for entity
		//auto it = std::find(group.second->begin(), group.second->end(), e);
		//if found erase it
		//if (it != group.second->end())
		//{
		//	group.second->erase(it);
		//}
	}
}

void GroupManager::remove(Entity e, string group)
{
	auto it = std::find(groups[group]->begin(), groups[group]->end(), e);
	//if found erase it
	if (it != groups[group]->end())
	{
		groups[group]->erase(it);
		if (groups[group]->size() == 0)
		{
			//delete groups[group];
			//groups.erase(group);
		}
	}
}

void GroupManager::removeGroup(string group)
{
	if (groups.find(group) == groups.end())
		return;

	groups[group]->clear();
	delete groups[group];
	groups.erase(group);
}

void GroupManager::removeAll()
{
	//loop through groups map
	for (auto group : groups)
	{
		removeGroup(group.first);
	}
}

int GroupManager::size()
{
	return groups.size();
}

int GroupManager::size(string group)
{
	if (groups.find(group) == groups.end())
		return 0;
	return groups[group]->size();
}
