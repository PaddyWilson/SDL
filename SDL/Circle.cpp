#include "Circle.h"

bool Circle::collision(Circle c)
{
	float dx = c.position.x - position.x;
	float dy = position.y - c.position.y;
	float r = radius + c.radius;
	return (dx*dx + dy * dy <= r * r);
}

bool Circle::collision(Vector2D v)
{
	float dx = std::abs(position.x - v.x);
	float dy = std::abs(position.y - v.y);
	float R = radius;

	return (dx*dx + dy * dy <= R * R);
}

bool Circle::collision(Rectanglef r)
{
	return r.collision(*this);
	//return false;
}

bool Circle::collision(struct Line l)
{
	// Circle centre point
	Vector2D P(position);

	float APx = P.x - l.point1.x;
	float APy = P.y - l.point1.y;

	float ABx = l.point2.x - l.point1.x;
	float ABy = l.point2.y - l.point1.y;

	float magAB2 = ABx * ABx + ABy * ABy;
	float ABdotAP = ABx * APx + ABy * APy;

	float t = ABdotAP / magAB2;

	// Closest point to the line l from Circle center
	Vector2D W;

	if (t < 0)	 W = Vector2D(l.point1.x, l.point1.y);
	else if (t > 1)  W = Vector2D(l.point2.x, l.point2.y);
	else			 W = Vector2D(l.point1.x + ABx * t, l.point1.y + ABy * t);

	float dist = P.getDistance(W);// Distance_PointPoint(P, W);

	// if the distance is less than the circle radius
	return dist < radius;
}
