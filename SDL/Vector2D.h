#pragma once

struct Line;
struct Circle;
template <typename T> struct RectangleBase;
using Rectanglef = RectangleBase<float>;

template<typename T>
struct Vector2DBase
{
	Vector2DBase() :x(0), y(0) {};
	Vector2DBase(T x, T y) :x(x), y(y) {};

	T x;
	T y;

	void move(T x, T y) { this->x += x; this->y += y; };
	void move(Vector2DBase<T> vec) { x += vec.x; y += vec.y; };
	void zero() { x = 0; y = 0; };

	float getDistance(Vector2DBase<T> b) {
		return sqrt(pow((b.x - x), 2) + pow((b.y - y), 2));
	}

	Vector2DBase<T> normalize(Vector2DBase<T> point) {
		return normalize(this, point);
	}

	static Vector2DBase<T> normalize(Vector2DBase<T> point1, Vector2DBase<T> point2) {
		Vector2DBase<T> move_dir = point1 - point2;//get dir to move in
		float dir_length = sqrt(pow(move_dir.x, 2) + pow(move_dir.y, 2)); //get distance between them
		return (move_dir / dir_length);//normalize(-1 to 1 value)
	}

	float distance(Vector2DBase<T> point) {
		return distance(*this, point);
	}

	static float distance(Vector2DBase<T> point1, Vector2DBase<T> point2) {
		Vector2DBase<T> move_dir = point1 - point2;//get dir to move in
		return sqrt(pow(move_dir.x, 2) + pow(move_dir.y, 2)); //get distance between them		
	}
	
	bool collision(RectangleBase<T> b)
	{
		Vector2DBase<float> vec((float)x, (float)y);
		return b.collision(vec);
	};

	bool collision(Circle C)
	{
		Vector2DBase<float> vec((float)x, (float)y);
		return C.collision(vec);
	};

	inline Vector2DBase<T> operator+(const Vector2DBase<T>& b) {
		Vector2DBase<T> vec;
		vec.x = this->x + b.x;
		vec.y = this->y + b.y;
		return vec;
	}

	inline void operator+=(const T& b) { this->x += b; this->y += b; }
	inline void operator+=(const Vector2DBase<T>& b) { this->x += b.x; this->y += b.y; }

	inline Vector2DBase<T> operator-(const Vector2DBase<T>& b) {
		Vector2DBase<T> vec;
		vec.x = this->x - b.x;
		vec.y = this->y - b.y;
		return vec;
	}
	inline Vector2DBase<T> operator-(const T& b) {
		Vector2DBase<T> vec;
		vec.x = x - b;
		vec.y = y - b;
		return vec;
	}

	inline void operator-=(const T& b) { this->x -= b; this->y -= b; }
	inline void operator-=(const Vector2DBase<T>& b) { this->x -= b.x; this->y -= b.y; }

	inline Vector2DBase<T> operator*(const Vector2DBase<T>& b) {
		Vector2DBase<T> vec;
		vec.x = this->x * b.x;
		vec.y = this->y * b.y;
		return vec;
	}

	inline Vector2DBase<T> operator*(const T& b) {
		Vector2DBase<T> vec;
		vec.x = this->x * b;
		vec.y = this->y * b;
		return vec;
	}

	inline void operator*=(const T& b) { this->x *= b; this->y *= b; }
	inline void operator*=(const Vector2DBase<T>& b) { this->x *= b.x; this->y *= b.y; }

	inline Vector2DBase<T> operator/(const Vector2DBase<T>& b) {
		Vector2DBase<T> vec;
		vec.x = this->x / b.x;
		vec.y = this->y / b.y;
		return vec;
	}

	inline Vector2DBase<T> operator/(const T& b) {
		Vector2DBase<T> vec;
		vec.x = this->x / b;
		vec.y = this->y / b;
		return vec;
	}

	inline void operator/=(const T& b) { this->x /= b; this->y /= b; }
	inline void operator/=(const Vector2DBase<T>& b) { this->x /= b.x; this->y /= b.y; }

	template <typename U>
	Vector2DBase<U> convert()
	{
		return Vector2DBase<U>((U)x,(U)y);
	}
};

using Vector2D = Vector2DBase<float>;
using Vector2Dd = Vector2DBase<double>;
using Vector2Di = Vector2DBase<int>;
using Vector2Dl = Vector2DBase<long>;
using Vector2Dui = Vector2DBase<unsigned int>;
using Vector2Dul = Vector2DBase<unsigned long>;
