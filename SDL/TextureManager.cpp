#include "TextureManager.h"

//global texture manager
TextureManager gTextureManager;

TextureManager::TextureManager()
{
	renderer = NULL;
}

TextureManager::~TextureManager()
{
	for (auto tex : textures)
	{
		if (tex.second != NULL)
		{
			delete tex.second;
			tex.second = NULL;
		}
	}
}

map<string, Texture*>* TextureManager::getTextureMap()
{
	return &textures;
}

void TextureManager::setRenderer(SDL_Renderer * renderer)
{
	this->renderer = renderer;
}

bool TextureManager::addTexture(string name, string path, bool fromContent)
{
	Texture* texture = new Texture();
	string filePath = "";

	if (fromContent)
		filePath = TEXTURE_PATH + path;
	else
		filePath = path;

	if (!texture->loadTexture(renderer, filePath))
	{
		cout << "Failed to load texture:" << name.c_str() << endl;
		return false;
	}

	//check if a texture with the same name exists
	if (textures[name] != NULL)
	{
		cout << "Texture name taken:" << name.c_str() << endl;
		return false;
	}

	//add texture
	textures[name] = texture;
	return true;
}

bool TextureManager::addTexture(string name, Texture * texture)
{
	//check if a texture with the same name exists
	if (textures[name] != NULL)
	{
		cout << "Texture name taken:" << name.c_str() << endl;
		return false;
	}

	//add texture
	textures[name] = texture;
	return true;
}

Texture* TextureManager::getTexture(string name)
{
	if (textures[name] != NULL)
		return textures[name];
	//cout << "Texture: No texture named " << name.c_str() << endl;
	return 0;
}

void TextureManager::removeTexture(string name)
{
	if (textures[name] != NULL)
	{
		delete textures[name];
		textures[name] = NULL;
	}
}

int TextureManager::getCount()
{
	return textures.size();
}
