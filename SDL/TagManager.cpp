#include "TagManager.h"

void TagManager::add(Entity e, string tag)
{
	if (tag != "")
		entities[tag] = e;
}

Entity TagManager::get(string tag)
{
	return entities[tag];
}

//EntityHandle TagManager::get(string tag)
//{
//	EntityHandle handle;
//	handle.entity = entities[tag];
//	handle.world = world;
//	return handle;
//}

void TagManager::remove(Entity e)
{
	for (map<string, Entity>::iterator it = entities.begin(); it != entities.end(); )
	{
		if ((it->second) == e)
		{
			entities.erase(it);
			break;
		}
		else
		{
			it++;
		}
	}
}

void TagManager::remove(string tag)
{
	entities.erase(tag);
}

std::vector<std::string> TagManager::getTagList()
{
	vector<string> output;

	for (auto item : entities)
	{
		output.push_back(item.first);
	}

	return output;
}

void TagManager::removeAll()
{
	entities.clear();
}
