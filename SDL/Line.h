#pragma once
#include <vector>
#include "Vector2D.h"
#include "Circle.h"
#include "Rectangle.h"

//forward declare becasue circular dependency
struct Circle;
template <typename T> struct RectangleBase;
using Rectanglef = RectangleBase<float>;

struct Line
{
	Line();
	Line(Vector2D p1, Vector2D p2);
	Line(float x1, float y1, float x2, float y2);

	void move(Vector2D vec) {
		point1 += vec;
		point2 += vec;
	}

	void movePoint1(Vector2D vec) {
		point1 += vec;
	}

	void movePoint2(Vector2D vec) {
		point2 += vec;
	}

	bool collision(Line line); 
	bool collisionLine(float x1, float y1, float x2, float y2);
	bool collision(class Circle circle);
	bool collision(Rectanglef rec);
	
	Vector2D point1;
	Vector2D point2;
};

//struct MultiLine
//{
//	MultiLine()	{	}
//
//	MultiLine(Vector2D p1, Vector2D p2)
//	{
//		points.push_back(p1);
//		points.push_back(p2);
//	}
//
//	void addPoint(Vector2D point)
//	{
//		points.push_back(point);
//	}
//
//	std::vector<Vector2D> points;
//};