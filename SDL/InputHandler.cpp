#include "InputHandler.h"

InputHandler::InputHandler()
{
	currentKeys = new Uint8[SDL_NUM_SCANCODES];
	previousKeys = new Uint8[SDL_NUM_SCANCODES];
}

InputHandler::~InputHandler() 
{
	delete[] currentKeys;
	delete[] previousKeys;

	for (auto &item : keyCommand)
		delete item.second;

	keyCommand.clear();
	keyCommandState.clear();
}

void InputHandler::update()
{
	//get new keyboard state
	memcpy(previousKeys, currentKeys, sizeof(Uint8)*SDL_NUM_SCANCODES);

	//for (int i = 0; i < SDL_NUM_SCANCODES; i++)
	//	previousKeys[i] = currentKeys[i];

	SDL_PumpEvents();
	memcpy(currentKeys, SDL_GetKeyboardState(NULL), sizeof(Uint8)*SDL_NUM_SCANCODES);

	KeyState keyState = None;
	for (int i = 0; i < SDL_NUM_SCANCODES; i++)
	{
		//key up
		if ((int)previousKeys[i] == 0 && (int)currentKeys[i] == 0)
		{
			keyState = KeyState::Up;
		}
		//key pressed
		else if ((int)previousKeys[i] == 0 && (int)currentKeys[i] == 1)
		{
			keyState = KeyState::Pressed;
		}
		//key down
		else if ((int)previousKeys[i] == 1 && (int)currentKeys[i] == 1)
		{
			keyState = KeyState::Down;
		}
		//key released
		else if ((int)previousKeys[i] == 1 && (int)currentKeys[i] == 0)
		{
			keyState = KeyState::Released;
		}

		//add command to command queue
		//if (keyCommandState[i] == keyState && keyCommand.count(i) > 0)
		if (keyCommandState[(SDL_Scancode)i] == keyState && keyCommand.count((SDL_Scancode)i) > 0)
		{
			commandQueue.emplace(keyCommand[(SDL_Scancode)i]);
		}
	}
}

void InputHandler::processCommands()
{
	while (commandQueue.size() > 0)
	{
		commandQueue.front()->execute();
		commandQueue.pop();
	}
}

void InputHandler::mapKey(SDL_Scancode key, ICommand * command)
{
	keyCommand[key] = command;
	keyCommandState[key] = KeyState::Down;
}

void InputHandler::mapKey(SDL_Scancode key, ICommand * command, KeyState state)
{
	keyCommand[key] = command;
	keyCommandState[key] = state;
}

void InputHandler::mapKey(SDL_Scancode key, std::function<void()> command)
{
	LamdaCommand *com = new LamdaCommand(command);
	keyCommand[key] = com;
	keyCommandState[key] = KeyState::Down;
}

void InputHandler::mapKey(SDL_Scancode key, std::function<void()> command, KeyState state)
{
	LamdaCommand *com = new LamdaCommand(command);
	keyCommand[key] = com;
	keyCommandState[key] = state;
}

void InputHandler::unmapKey(SDL_Scancode key)
{
	if (keyCommand.count(key) > 0)
	{
		std::map<SDL_Scancode, ICommand*>::iterator iter = keyCommand.find(key);
		keyCommand.erase(iter);
	}
}


