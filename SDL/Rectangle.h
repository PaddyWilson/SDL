#pragma once

#undef min
#undef max

#include <algorithm>
#include "Vector2D.h"
#include "Circle.h"
#include "Line.h"

struct Line;
struct Circle;

template <typename T>
struct RectangleBase
{
	RectangleBase() : x(0), y(0), width(0), height(0) {};
	RectangleBase(T x, T y, T width, T height) : x(x), y(y), width(width), height(height) {};
	RectangleBase(Vector2D vec, Vector2D vec2) : x(vec.x), y(vec.y), width(vec2.x), height(vec2.y) {};
	T x;
	T y;
	T width;
	T height;

	Vector2D getTopLeft() { return Vector2D(x, y); };
	Vector2D getBottomRight() { return Vector2D(width, height); };

	inline void move(T x, T y) { x += x; y += y; };
	inline void move(Vector2D vec) { x += vec.x; y += vec.y; };

	inline void set(RectangleBase<T> rec)
	{
		x = rec.x;
		y = rec.y;
		width = rec.width;
		height = rec.height;
	};

	/*bool collision(Vector2D b);
	bool collision(RectangleBase<T> b);
	bool collision(class Circle b);
	bool collision(class Line b);*/

	//should put in cpp file
	RectangleBase<T> getIntersect(RectangleBase<T> b)
	{
		RectangleBase<T> intersect;

		T Axmin = x;
		T Axmax = x + width;
		T Aymax = y + height;
		T Aymin = y;

		T Bxmin = b.x;
		T Bxmax = b.x + b.width;
		T Bymax = b.y + b.height;
		T Bymin = b.y;

		intersect.x = std::max(Axmin, Bxmin);
		intersect.y = std::max(Aymin, Bymin);
		intersect.width = std::min(Axmax, Bxmax) - intersect.x;
		intersect.height = std::min(Aymax, Bymax) - intersect.y;

		return intersect;
	};

	float getDistance(RectangleBase<T> b)
	{
		return std::sqrt(std::pow((B.x - x), 2) + std::pow((B.y - y), 2));
	};

	Vector2D getCenterPoint()
	{
		return Vector2D(x + (width / 2), y + (height / 2));
	};

	bool collision(Vector2D p)
	{
		return (p.x >= this->x) && (p.x < this->x + this->width) &&
			(p.y >= this->y) && (p.y < this->y + this->height);
	};

	bool collision(RectangleBase<T> b)
	{
		T Axmin = x;
		T Axmax = x + width;
		T Aymax = y + height;
		T Aymin = y;

		T Bxmin = b.x;
		T Bxmax = b.x + b.width;
		T Bymax = b.y + b.height;
		T Bymin = b.y;

		// Collision check
		if (Axmax < Bxmin || Axmin > Bxmax) return false;
		if (Aymax < Bymin || Aymin > Bymax) return false;

		return true;
	};

	bool collision(struct Circle C)
	{
		float Dx = C.position.x - std::max(x, std::min(C.position.x, x + width));
		float Dy = C.position.y - std::max(y, std::min(C.position.y, y + height));

		return (Dx * Dx + Dy * Dy) < (C.radius * C.radius);
	};

	bool collision(struct Line L);

	bool operator==(const RectangleBase<T> &r) const {
		return x == x && y == y && width == width && height == height;
	}
	bool operator!=(const RectangleBase<T> &r) const {
		return !(this == r);
	}

	//auto operator<=>(const RectangleBase<T>&) = default;
};

template<typename T>
inline bool RectangleBase<T>::collision(struct Line L)
{
	float Xmin, Xmax, Ymin, Ymax;
	Xmin = x;
	Xmax = x + width;
	Ymin = y;
	Ymax = y + height;

	if (L.point1.x < Xmin && L.point2.x < Xmin) return false;
	if (L.point1.y < Ymin && L.point2.y < Ymin) return false;
	if (L.point1.x > Xmax && L.point2.x > Xmax) return false;
	if (L.point1.y > Ymax && L.point2.y > Ymax) return false;

	// check every side of the rectangle
	if (L.collisionLine(Xmin, Ymin, Xmax, Ymin)) return true;
	if (L.collisionLine(Xmin, Ymin, Xmin, Ymax)) return true;
	if (L.collisionLine(Xmax, Ymin, Xmax, Ymax)) return true;
	if (L.collisionLine(Xmin, Ymax, Xmax, Ymax)) return true;

	return false;
};

using Rectanglef = RectangleBase<float>;
using Rectangled = RectangleBase<double>;
using Rectanglei = RectangleBase<int>;
using Rectangleui = RectangleBase<unsigned int>;
using Rectanglel = RectangleBase<long>;
using Rectangleul = RectangleBase<unsigned long>;
