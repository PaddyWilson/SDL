#pragma once

#include <map>
#include <vector>
#include <string>

#include "Sprite.h"

class SpriteManager
{
public:
	SpriteManager();
	~SpriteManager();

	Sprite* create(std::string name, std::string texture, int x = 0, int y = 0, int width = 0, int height = 0);
	//Sprite* create(std::string name, std::string texture, SDL_Rect subRect = { 0, 0, 0, 0 });

	void addSprite(std::string name, Sprite &sprite);
	Sprite* getSprite(std::string name);
	void removeSprite(std::string name);

	int getCount();
	std::vector<std::string>* getNames();
private:
	std::map<std::string, Sprite> sprites;
	std::vector<std::string> spriteNames;
};

extern SpriteManager gSpriteManager;

