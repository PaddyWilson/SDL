#include "Animation.h"

Animation::Animation(bool loop)
{
	loops = loop;
	stoped = true;
	paused = false;
}

void Animation::addFrame(Sprite * sprite, int time, Vector2D offset)
{
	AnimationFrame frame;
	frame.sprite = sprite;
	frame.offset = offset;
	frame.time = time;
	frames.push_back(frame);
	
	if (frames.size() == 1)
	{
		ticks = time;
	}
}

AnimationFrame * Animation::getNextFrame()
{
	int time = timer.getTicks();

	if (!paused && !stoped) {
		while (ticks < time)
		{
			currentFrame++;

			//loop or stay at end
			if (currentFrame == frames.size()) {
				if (loops) {
					currentFrame = 0;
					ticks = frames[currentFrame].time;
					timer.restart();
					time = 0;
				}
				else {
					currentFrame--;
					return &frames[currentFrame];
				}
			}
			else
			{
				ticks += frames[currentFrame].time;
			}
		}
	}

	return &frames[currentFrame];
}

void Animation::start()
{
	if (stoped)
	{
		stoped = false;
		paused = false;
		timer.start();
	}
	else if (paused)
	{
		stoped = false;
		paused = false;
		timer.unpause();
	}
}

void Animation::stop()
{
	if (!stoped)
	{
		stoped = true;
		currentFrame = 0;
		ticks = frames[currentFrame].time;
		timer.stop();
	}
}

void Animation::pause()
{
	if (!paused)
	{
		paused = true;
		timer.pause();
	}
	else if (paused)
	{
		paused = false;
		timer.unpause();
	}
}

void Animation::restart()
{
	currentFrame = 0;
	ticks = frames[currentFrame].time;
	paused = false;
	stoped = false;
	timer.restart();
}
