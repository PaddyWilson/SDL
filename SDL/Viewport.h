#pragma once

#include <SDL.h>

class Viewport
{
public:
	Viewport(int x, int y, int width, int height);
	~Viewport();

	void switchView(SDL_Renderer*renderer);

	int getX();
	int getY();
	int getWidth();
	int getHeight();

private:
	SDL_Rect view;
};

