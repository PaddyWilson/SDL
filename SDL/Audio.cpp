#include "Audio.h"

Music::Music(int loop, int channel) :
	loop(loop), channel(channel)
{}

Music::~Music()
{
	Mix_FreeMusic(music);
	music = NULL;
}

void Music::play()
{
	Mix_PlayMusic(music, loop);
}

void Music::pause()
{
	Mix_PauseMusic();
}

void Music::resume()
{
	Mix_ResumeMusic();
}

void Music::stop()
{
	Mix_HaltMusic();
}

bool Music::load(std::string path)
{
	music = Mix_LoadMUS(path.c_str());
	if (music == NULL)
	{
		printf("Failed to load beat music! SDL_mixer Error: %s\n", Mix_GetError());
		return false;
	}
	//std::cout << "Loaded Audio:" << path << std::endl;
	return true;
}

/****************************************/

SoundEffect::SoundEffect(int loop, int channel) :
	Music(loop, channel)
{}

SoundEffect::~SoundEffect()
{
	Mix_FreeChunk(sound);
	sound = NULL;
}

void SoundEffect::play()
{
	Mix_PlayChannel(channel, sound, loop);
}

bool SoundEffect::load(std::string path)
{
	sound = Mix_LoadWAV(path.c_str());
	if (sound == NULL)
	{
		printf("Failed to load beat music! SDL_mixer Error: %s\n", Mix_GetError());
		return false;
	}
	//std::cout << "Loaded Audio:" << path << std::endl;
	return true;
}