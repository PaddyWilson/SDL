#pragma once

#include <SDL.h>
#include <SDL_image.h>
#include <iostream>

class Texture
{
private:
	SDL_Texture * texture;

	int width;
	int height;

public:
	Texture();
	~Texture();

	bool loadTexture(SDL_Renderer* render, std::string path);
	void render(SDL_Renderer* render, int x, int y, SDL_Rect* clip = NULL);

	void setTexture(SDL_Texture* tex, int width, int height) {
		texture = tex; this->width = width; this->height = height;
	};
	SDL_Texture* getTexture() { return texture; };

	int getWidth();
	int getHeight();

private:
	//Deallocates texture
	void free();
};

