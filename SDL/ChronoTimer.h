#pragma once
#include <chrono>
using namespace std::chrono;
class ChronoTimer
{
public:
	ChronoTimer():timePrev(high_resolution_clock::now()) {
	};
	
	double GetDelta()
	{
		// 1. Get current time as a std::chrono::time_point
		auto timeCurrent = high_resolution_clock::now();

		// 2. Get the time difference as seconds 
		// ...represented as a double
		duration< double > delta(timeCurrent - timePrev);

		// 3. Reset the timePrev to the current point in time
		timePrev = high_resolution_clock::now();

		// 4. Returns the number of ticks in delta
		return delta.count();
	}


private:
	time_point<high_resolution_clock> timePrev;
};
