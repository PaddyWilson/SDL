#include "SCollision.h"

SCollision::SCollision(Camera * cam) : grid(100, 100), staticGrid(100, 100)
{
	camera = cam;
	requiredComponents.set(getComponentID<Transform>(), true);
	requiredComponents.set(getComponentID<BaseCollider>(), true);
}

SCollision::~SCollision() {}

void SCollision::init() {}

//for testing
static int frameCount = 0;
void SCollision::update(double deltaTime)
{
	//cout << "Collision System EC:" << entities.size() << endl;
	vector<CollisionInfo> collisions;
	vector<EntityInfo> infoList;

	int moveId = getComponentID<Movement>();

	//size 102400
	//insert info into quadtree
	//quadtree.clear();
	grid.clear();
	for (size_t i = 0; i < entities.size(); i++)
	{
		EntityHandle e;
		e.world = world;
		e.entity = entities[i];

		EntityInfo info;
		info.entity = e;
		info.transform = e.getComponent<Transform>();
		info.collider = e.getComponent<BaseCollider>();

		Rectanglef rec = info.collider->getAABB(info.transform->position);
		//debug drawing
		//debugDraw(info.collider, info.transform);

		//quadtree.insert(rec, info);
		grid.insert(rec, info);
		infoList.push_back(info);
	}
	//grid.debugDraw();
	//staticGrid.debugDraw();

	//keeps track of hit between entities
	//map<int, vector<int>> allReadyHit;
	allReadyHit.clear();

	//find collisions
	int hitCount = 0;
	int staticHitCount = 0;

	//vector<EntityInfo>hits;
	//hits.reserve(200);
	//vector<EntityInfo>staticHits;
	//staticHits.reserve(15000);

	for (size_t i = 0; i < infoList.size(); i++)
	{
		hits.clear();
		staticHits.clear();

		EntityHandle e1 = infoList[i].entity;
		BaseCollider *base1 = infoList[i].collider;
		Transform *t1 = infoList[i].transform;

		//search grids for collisions
		//vector<EntityInfo>hits;
		//vector<EntityInfo>staticHits;
		grid.retrieve(base1->getAABB(t1->position), &hits);
		staticGrid.retrieve(base1->getAABB(t1->position), &staticHits);

		hitCount = hits.size();
		staticHitCount = staticHits.size();
		//cout << e1.entity.id << " Hits:" << hitCount << " Potential:" << hits.size() << " sHits:" << staticHitCount << " Potential:" << staticHits.size() << endl;

		//test collisions for moveable entities
		for (size_t x = 0; x < hits.size(); x++)
		{
			if (e1.entity.id == hits[x].entity.entity.id) continue;//hit it self. ignore

			EntityHandle e2 = hits[x].entity;
			BaseCollider *base2 = hits[x].collider;
			Transform *t2 = hits[x].transform;

			if (base1->getAABB(t1->position).collision(base2->getAABB(t2->position)))
			{
				//check for things that have alread collided
				if (std::find(allReadyHit[e1.entity.id].begin(), allReadyHit[e1.entity.id].end(), e2.entity.id) != allReadyHit[e1.entity.id].end() ||
					std::find(allReadyHit[e2.entity.id].begin(), allReadyHit[e2.entity.id].end(), e1.entity.id) != allReadyHit[e2.entity.id].end())
				{
					//cout << "Already collided" << endl;
				}
				else//the two entities have never collided
				{
					//add them to the map
					allReadyHit[e1.entity.id].push_back(e2.entity.id);
					allReadyHit[e2.entity.id].push_back(e1.entity.id);

					collisions.push_back(CollisionInfo(e1, base1, t1, e2, base2, t2));
					//hitCount++;
					//cout << "Box Collision " << hitCount << endl;
				}
			}
		}
		//test collisions for non moveable entities
		for (size_t x = 0; x < staticHits.size(); x++)
		{
			if (e1.entity.id == staticHits[x].entity.entity.id) continue;//hit it self. ignore

			EntityHandle e2 = staticHits[x].entity;
			BaseCollider *base2 = staticHits[x].collider;
			Transform *t2 = staticHits[x].transform;

			if (base1->getAABB(t1->position).collision(base2->getAABB(t2->position)))
			{
				//check for things that have alread collided
				if (std::find(allReadyHit[e1.entity.id].begin(), allReadyHit[e1.entity.id].end(), e2.entity.id) != allReadyHit[e1.entity.id].end() ||
					std::find(allReadyHit[e2.entity.id].begin(), allReadyHit[e2.entity.id].end(), e1.entity.id) != allReadyHit[e2.entity.id].end())
				{
					continue;//cout << "Already collided" << endl;
				}
				//else//the two entities have never collided
				{
					//add them to the map
					allReadyHit[e1.entity.id].push_back(e2.entity.id);
					allReadyHit[e2.entity.id].push_back(e1.entity.id);

					collisions.push_back(CollisionInfo(e1, base1, t1, e2, base2, t2));
					//hitCount++;
					//cout << "Box Collision " << hitCount << endl;
				}
			}
		}
	}

	//cout << "Dealing with " << collisions.size() << " Collisions. " << "Hits:" << hitCount << endl;
	int resolved = 0;
	for (size_t i = 0; i < collisions.size(); i++)
	{
		EntityHandle e1 = collisions[i].e1;
		EntityHandle e2 = collisions[i].e2;

		BaseCollider* b1 = collisions[i].col1;
		BaseCollider* b2 = collisions[i].col2;

		Transform *t1 = collisions[i].t1;
		Transform *t2 = collisions[i].t2;

		if (b1->interects(t1->position, b2, t2->position))
		{
			resolved++;
			eventSystem->publish(new CollisionEvent(e1, e2));
		}
	}
	//cout << frameCount << "* AABB Hits:" << collisions.size() << " Resolved:" << resolved << " " << hitCount << ":" << staticHitCount << endl;
	frameCount++;
}

void SCollision::addEntity(Entity e)
{
	if (hasComponents(e))
	{
		//does not move so add it to a static quadtree and static entity vector
		if (!world->getEntityBitmask(e).test(getComponentID<Movement>()))
		{
			EntityHandle handle;
			handle.world = world;
			handle.entity = e;

			EntityInfo info;
			info.entity = handle;
			info.transform = handle.getComponent<Transform>();
			info.collider = handle.getComponent<BaseCollider>();

			Rectanglef rec = info.collider->getAABB(info.transform->position);
			staticGrid.insert(rec, info);
			staticEntities.push_back(e);
		}
		//add to normal entity list
		else {
			entities.push_back(e);
		}
	}
}

void SCollision::removeEntity(Entity e)
{
	if (hasComponents(e))
	{
		//does not move so add it to a static quadtree and static entity vector
		if (!world->getEntityBitmask(e).test(getComponentID<Movement>()))
		{
			EntityHandle handle;
			handle.world = world;
			handle.entity = e;

			EntityInfo info;
			info.entity = handle;
			info.transform = handle.getComponent<Transform>();
			info.collider = handle.getComponent<BaseCollider>();

			auto it = std::find(staticEntities.begin(), staticEntities.end(), e);

			if (it != staticEntities.end())
			{
				staticEntities.erase(it);
				staticGrid.remove(info);
			}
		}
		//remove from normal entity list
		else {
			
			ISystem::removeEntity(e);
		}
	}
}

void SCollision::debugDraw(BaseCollider * collider, Transform * trans)
{
	Vector2D position = trans->position;

	SDL_Rect rec;
	Rectanglef aabb;

	Vector2D pos, pos2;

	switch (collider->type)
	{
	case BoxType:
		pos = position + collider->offset;
		rec.x = pos.x;
		rec.y = pos.y;
		rec.w = collider->size.x;
		rec.h = collider->size.y;
		drawRectangle(gRenderer, rec.x, rec.y, rec.w, rec.h, 0x00, 0x00, 0xFF, 0xFF);
		break;
	case CircleType:
		aabb = collider->getAABB(position);
		rec.x = aabb.x;
		rec.y = aabb.y;
		rec.w = aabb.width;
		rec.h = aabb.height;
		drawRectangle(gRenderer, rec.x, rec.y, rec.w, rec.h, 255, 127, 0, 255);

		pos = position + collider->offset;
		drawCircle(gRenderer, pos.x, pos.y, collider->radius, 0x00, 0x00, 0xFF, 0xFF);
		break;
	case PointType:
		pos = position + collider->offset;
		drawPoint(gRenderer, pos, Color());
		break;
	case LineType:
		aabb = collider->getAABB(position);
		rec.x = aabb.x;
		rec.y = aabb.y;
		rec.w = aabb.width;
		rec.h = aabb.height;
		drawRectangle(gRenderer, rec.x, rec.y, rec.w, rec.h, 255, 127, 0, 255);

		pos = position + collider->point1 + collider->offset;
		pos2 = position + collider->point2 + collider->offset;
		SDL_SetRenderDrawColor(gRenderer, 0x00, 0x00, 0xFF, 0xFF);
		SDL_RenderDrawLine(gRenderer, pos.x, pos.y, pos2.x, pos2.y);
		break;
	default:
		break;
	}
}
